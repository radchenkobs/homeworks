package HappyFamily.app.stepper;

import libs.Parser;

public class StepRadio extends Step<Boolean> {

    public StepRadio(String title) {
        super(title, Parser::parseBooleanFromString);
    }
}

