package HappyFamily.app.parts.createNewFamily;

import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.InsertTestData;
import HappyFamily.app.parts.Part;
import HappyFamily.family.FamilyController;
import HappyFamily.humans.Man;
import HappyFamily.humans.Woman;
import libs.Console;
import libs.Logger;

public class CreateNewFamily extends Part {

    public CreateNewFamily(FamilyController controller) {
        super("Create new family", controller);
    }

    private Woman createMother() {
        Console.title("Mother:\n");
        Logger.info("Create mother");
        HumanStepCreator creatorMother = new HumanStepCreator();
        Woman mother = new Woman(
                creatorMother.getData().name(),
                creatorMother.getData().surname(),
                creatorMother.getData().birthday(),
                null,
                creatorMother.getData().iq(),
                null
        );
        Console.print(mother.prettyFormat());
        Console.printLine();
        Logger.info(String.format("Mother created: %s", mother.prettyFormat()));
        return mother;
    }

    private Man createFather() {
        Console.title("Father:\n");
        Logger.info("Create father");
        HumanStepCreator creatorFather = new HumanStepCreator();
        Man father = new Man(
                creatorFather.getData().name(),
                creatorFather.getData().surname(),
                creatorFather.getData().birthday(),
                null,
                creatorFather.getData().iq(),
                null
        );
        Console.print(father.prettyFormat());
        Console.printLine();
        Logger.info(String.format("Father created: %s", father.prettyFormat()));
        return father;
    }

    public static IPart of(FamilyController controller) {
        return new CreateNewFamily(controller).instance();
    }

    @Override
    public void run() {

        Woman mother = createMother();
        Man father = createFather();

        controller.createNewFamily(mother, father);
    }
}