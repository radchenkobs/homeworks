package HappyFamily.app.menu;

import HappyFamily.app.parts.*;
import HappyFamily.app.parts.deleteFamily.DeleteFamilyById;
import HappyFamily.app.parts.deleteFamily.DeleteFamilyByIndex;
import HappyFamily.family.FamilyController;

import java.util.HashMap;
import java.util.Map;

public class DeleteFamilyMenu extends Menu {

    public DeleteFamilyMenu(FamilyController controller) {
        super("Delete family menu");

        Map<Integer, IPart> menu = new HashMap<>();

        menu.put(1, DeleteFamilyById.of(controller));
        menu.put(2, DeleteFamilyByIndex.of(controller));

        setMenu(menu);
    }
}
