package HappyFamily.app.parts.createNewFamily;

import HappyFamily.app.stepper.Step;
import HappyFamily.app.stepper.Stepper;
import HappyFamily.constants.HumanConstants;
import libs.DateUtil;
import libs.Input;
import libs.Parser;

public class HumanStepCreator {
    private final Stepper steps = new Stepper();

    public record HumanData(String name, String surname, long birthday, int iq) {
    }

    public HumanStepCreator() {
        steps.add(new Step<>("Enter name"));
        steps.add(new Step<>("Enter surname"));
        steps.add(new Step<Integer>("Enter year", (s -> Parser.parseIntFromString(s, HumanConstants.HUMAN_MIN_YEAR, HumanConstants.HUMAN_MAX_YEAR))));
        steps.run();

        Integer year = (Integer) steps.get(2).getValidateValue();
        DateUtil currentDate = DateUtil.of();
        int maxMonth = currentDate.getYear() == year ? currentDate.getMonth() : 12;
        steps.add(new Step<Integer>("Enter month", (s -> Parser.parseIntFromString(s, 1, maxMonth))));
        steps.runFrom(2);

        Integer month = (Integer) steps.get(3).getValidateValue();
        int maxDaysInMonth = DateUtil.lengthOfMonth(year, month);
        int maxDays = currentDate.getYear() == year && currentDate.getMonth() == month ? currentDate.getDayOfMonth() : maxDaysInMonth;
        steps.add(new Step<Integer>("Enter day", (s -> Parser.parseIntFromString(s, 1, maxDays))));
        steps.add(new Step<Integer>("Enter iq", (s -> Parser.parseIntFromString(s, HumanConstants.HUMAN_MIN_IQ, HumanConstants.HUMAN_MAX_IQ))));
        steps.runFrom(3);
    }

    public HumanData getData() {
        String name = steps.get(0).getValue();
        String surname = steps.get(1).getValue();
        Integer year = (Integer) steps.get(2).getValidateValue();
        Integer month = (Integer) steps.get(3).getValidateValue();
        Integer day = (Integer) steps.get(4).getValidateValue();
        Integer iq = (Integer) steps.get(5).getValidateValue();
        long birthDay = DateUtil.of(year, month, day).getTimestamp();
        return new HumanData(name, surname, birthDay, iq);
    }
}
