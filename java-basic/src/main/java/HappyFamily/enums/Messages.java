package HappyFamily.enums;

public enum Messages {
    OOPS("Oops! Something wrong..."),
    OUT_OF_BOUNDS("Out of bounds"),
    EMPTY_STRING("Empty string"),
    INVALID_PET_NAME("Invalid pet name"),
    INVALID_PET_TRICK_LEVEL("Invalid pet trickLevel"),
    INVALID_PET_AGE("Invalid pet age"),
    INVALID_HUMAN_YEAR("Invalid human year"),
    INVALID_HUMAN_IQ("Invalid human IQ");

    public String message;

    Messages(String message) {
        this.message = message;
    }
}
