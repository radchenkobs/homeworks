package HappyFamily.app.parts.editFamily.adoptChild;

import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.InsertTestData;
import HappyFamily.app.parts.Part;
import HappyFamily.family.FamilyController;
import HappyFamily.humans.Human;
import HappyFamily.humans.Man;
import HappyFamily.humans.Woman;
import libs.Console;
import libs.Logger;

public class AdoptChild extends Part {
    public AdoptChild(FamilyController controller) {
        super("Adopt child", controller);
    }

    public static IPart of(FamilyController controller) {
        return new AdoptChild(controller).instance();
    }

    @Override
    public void run() {
        AdoptChildStepper adoptChildStepper = new AdoptChildStepper(controller);
        int index = adoptChildStepper.getData().index();
        boolean isMale = adoptChildStepper.getData().isMale();
        String name = adoptChildStepper.getData().name();
        String surname = adoptChildStepper.getData().surname();
        String birthday = adoptChildStepper.getData().birthday();
        int iq = adoptChildStepper.getData().iq();
        Human c;
        if (isMale) {
            c = new Man(name, surname, birthday, iq);
        } else {
            c = new Woman(name, surname, birthday, iq);
        }
        controller.adoptChild(index, c);
        String msg = String.format("Adopt child: %s", c.prettyFormat());
        Console.print(msg + "\n");
        Logger.info(String.format("Child is adopt. %s", msg));

    }
}
