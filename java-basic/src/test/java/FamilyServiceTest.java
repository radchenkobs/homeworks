import HappyFamily.constants.Names;
import HappyFamily.family.Family;
import HappyFamily.family.FamilyService;
import HappyFamily.humans.Man;
import HappyFamily.humans.Woman;
import HappyFamily.pet.Cat;
import HappyFamily.pet.Pet;
import libs.DateUtil;
import libs.Logger;
import libs.RandomNum;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import static org.junit.Assert.*;

public class FamilyServiceTest {
    public FamilyService service;

    public long getRandomTimestamp() {
        int year = RandomNum.random(1970, 1980);
        int month = RandomNum.random(1, 12);
        int day = RandomNum.random(1, 28);
        return DateUtil.of(year, month, day).getTimestamp();
    }

    @Before
    public void starter() {
        Logger.disabled = true;
        service = new FamilyService();
        Man man1 = new Man(
                Names.getRandomMaleName(),
                "Karleone",
                getRandomTimestamp(),
                null,
                RandomNum.random(100, 180),
                null
        );
        Man man2 = new Man(
                Names.getRandomMaleName(),
                "Rossi",
                getRandomTimestamp(),
                null,
                RandomNum.random(100, 180),
                null
        );
        Woman woman1 = new Woman(
                Names.getRandomFemaleName(),
                "Karleone",
                getRandomTimestamp(),
                null,
                RandomNum.random(100, 180),
                null
        );

        Woman woman2 = new Woman(
                Names.getRandomFemaleName(),
                "Rossi",
                getRandomTimestamp(),
                null,
                RandomNum.random(100, 180),
                null
        );
        Family family1 = new Family(woman1, man1);
        service.createNewFamily(family1);
        service.createNewFamily(woman2, man2);
    }

    @Test
    public void getAllFamiliesTest() {
        List<Family> res = service.getAllFamilies();
        int exp = 2;
        assertEquals("FamilyService count test getAllFamilies", exp, res.size());
    }

    @Test
    public void getFamiliesBiggerThanTest() {
        List<Family> resOne = service.getFamiliesBiggerThan(2);
        int expOne = 2;
        List<Family> resTwo = service.getFamiliesBiggerThan(3);
        int expTwo = 0;
        assertEquals("FamilyService count test getFamiliesBiggerThan(2)", expOne, resOne.size());
        assertEquals("FamilyService count test getFamiliesBiggerThan(3)", expTwo, resTwo.size());
    }

    @Test
    public void getFamiliesLessThanTest() {
        List<Family> resOne = service.getFamiliesLessThan(2);
        int expOne = 2;
        List<Family> resTwo = service.getFamiliesLessThan(1);
        int expTwo = 0;
        assertEquals("FamilyService count test getFamiliesLessThan(2)", expOne, resOne.size());
        assertEquals("FamilyService count test getFamiliesLessThan(1)", expTwo, resTwo.size());
    }

    @Test
    public void countFamiliesWithMemberNumberTest() {
        int resOne = service.countFamiliesWithMemberNumber(2);
        int expOne = 2;
        int resTwo = service.countFamiliesWithMemberNumber(3);
        int expTwo = 0;
        assertEquals("FamilyService count test countFamiliesWithMemberNumber(2)", expOne, resOne);
        assertEquals("FamilyService count test countFamiliesWithMemberNumber(3)", expTwo, resTwo);
    }

    @Test
    public void createNewFamilyTest() {
        Man m = new Man(
                Names.getRandomMaleName(),
                "Karleone",
                RandomNum.random(1970, 1980),
                null,
                RandomNum.random(100, 180),
                null
        );
        Woman w = new Woman(
                Names.getRandomFemaleName(),
                "Karleone",
                RandomNum.random(1970, 1980),
                null,
                RandomNum.random(100, 180),
                null
        );
        int initSize = service.getAllFamilies().size();
        service.createNewFamily(w, m);
        int sizeAfterCreate = service.getAllFamilies().size();
        assertEquals("FamilyService count test createNewFamilyTest", initSize + 1, sizeAfterCreate);
    }

    @Test
    public void deleteFamilyByIndexTest() {
        Family family = service.getFamilyByIndex(0);
        int init = service.getAllFamilies().stream().filter(f -> f.equals(family)).toList().size();
        boolean status = service.deleteFamilyByIndex(0);
        int afterDelete = service.getAllFamilies().stream().filter(f -> f.equals(family)).toList().size();
        assertTrue("FamilyService count test deleteFamilyByIndex return", status);
        assertEquals("FamilyService count test deleteFamilyByIndex deleted", afterDelete, init - 1);
    }

    @Test
    public void bornChildTest() {
        Family family = service.getFamilyByIndex(0);
        int init = family.getChildren().size();
        service.bornChild(family, "Male", "Female");
        int afterBorn = family.getChildren().size();
        assertEquals("FamilyService count test bornChild", afterBorn, init + 1);
    }

    @Test
    public void adoptChildTest() {
        Family family = service.getFamilyByIndex(0);
        int init = family.getChildren().size();
        Man child = new Man(
                Names.getRandomMaleName(),
                "Karleone",
                RandomNum.random(1970, 1980),
                null,
                RandomNum.random(100, 180),
                null
        );
        service.adoptChild(family, child);
        int afterBorn = family.getChildren().size();
        assertEquals("FamilyService count test adoptChild", afterBorn, init + 1);
    }

    @Test
    public void deleteAllChildrenOlderThenTest() {
        Family family = service.getFamilyByIndex(0);
        int init = family.getChildren().size();
        Man child = new Man(
                Names.getRandomMaleName(),
                "Karleone",
                RandomNum.random(1970, 1980),
                null,
                RandomNum.random(100, 180),
                null
        );
        service.adoptChild(family, child);
        service.deleteAllChildrenOlderThen(10);
        int afterBorn = family.getChildren().size();
        assertEquals("FamilyService count test deleteAllChildrenOlderThenTest", afterBorn, init);
    }

    @Test
    public void countTest() {
        int init = service.count();
        int exp = 2;
        assertEquals("FamilyService count test count", exp, init);
    }

    @Test
    public void getFamilyByIdTest() {
        Woman w = new Woman(
                Names.getRandomFemaleName(),
                "Karleone",
                RandomNum.random(1970, 1980),
                null,
                RandomNum.random(100, 180),
                null
        );

        Man m = new Man(
                Names.getRandomMaleName(),
                "Karleone",
                RandomNum.random(1970, 1980),
                null,
                RandomNum.random(100, 180),
                null
        );
        Family family = new Family(w, m);
        service.createNewFamily(family);
        Family res = service.getFamilyByIndex(2);
        assertEquals("FamilyService count test getFamilyById", res, family);
    }

    @Test
    public void addPetTest() {
        int petsInit = service.getPets(0).size();
        Cat cat = new Cat("Cat");
        service.addPet(0, cat);
        int petsAfterAdd = service.getPets(0).size();
        assertEquals("FamilyService count test addPet", petsInit + 1, petsAfterAdd);
    }

    @Test
    public void getPetsTest() {
        Set<Pet> pets = service.getPets(0);
        assertEquals("FamilyService count test getPets", 0, pets.size());
    }
}
