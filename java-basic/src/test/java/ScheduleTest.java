import HappyFamily.Schedule;
import HappyFamily.enums.DayOfWeek;
import org.junit.Test;

import static junit.framework.Assert.*;

public class ScheduleTest {

    @Test
    public void notNull() {
        Schedule schedule = new Schedule();
        assertNotNull("schedule is not null", schedule);
    }

    @Test
    public void setTaskTrue() {
        Schedule schedule = new Schedule();
        DayOfWeek day = DayOfWeek.MONDAY;
        String task = "test";
        boolean state = schedule.setTask(day, task);
        String msg = String.format("task: %s for %s day is set", task, day);
        assertTrue(msg, state);
    }

    @Test
    public void setTaskFalse() {
        Schedule schedule = new Schedule();
        DayOfWeek day = DayOfWeek.WEDNESDAY;
        String task = "test";
        schedule.setTask(day, task);
        boolean state = schedule.setTask(day, task);
        String msg = String.format("task: %s for %s dat already exist", task, day);
        assertFalse(msg, state);
    }

    @Test
    public void getTask() {
        Schedule schedule = new Schedule();
        DayOfWeek day = DayOfWeek.MONDAY;

        String[] empty = schedule.getTasks(day);
        String emptyMessage = String.format("Empty tasks for %s", day);
        assertEquals(emptyMessage, 0, empty.length);

        schedule.setTask(day, "test");
        String[] notEmpty = schedule.getTasks(day);
        String notEmptyMessage = String.format("Tasks for %s", day);
        assertEquals(notEmptyMessage, 1, notEmpty.length);
    }

    @Test
    public void toStringEmpty() {
        Schedule schedule = new Schedule();
        String msg = schedule.toString();
        String expected = "none";
        assertEquals("toString empty object state", expected, msg);
    }

    @Test
    public void toStringHalf() {
        Schedule schedule = new Schedule();
        for (int i = 0; i < DayOfWeek.values().length / 2; i++) {
            schedule.setTask(DayOfWeek.values()[i], String.format("task_%s_1", DayOfWeek.values()[i]));
        }
        String msg = schedule.toString();
        String expected = "{Monday (1): [task_MONDAY_1]; Tuesday (1): [task_TUESDAY_1]; Wednesday (1): [task_WEDNESDAY_1]}";
        assertEquals("toString half object state", expected, msg);
    }
}
