package HappyFamily.app;

import HappyFamily.family.Family;
import HappyFamily.helpers.UserInMessage;
import libs.Input;
import libs.Logger;
import libs.Parser;

import java.util.List;

public class AppController {

    private static List<Family> lastLoadingData = null;
    private static List<Family> lastSavingData = null;

    private static boolean confirmExit() {
        UserInMessage.confirmExit();
        String str = Input.string().trim();
        return Parser.parseStringIsYes(str);
    }

    public static boolean exit() {
        if (!confirmExit()) return false;
        Logger.exit();
        Logger.save();
        Logger.displayLog();
        System.exit(0);
        return true;
    }

    public static void setLastLoadingData(List<Family> loadingData) {
        AppController.lastLoadingData = loadingData;
    }

    public static List<Family> getLastLoadingData() {
        return AppController.lastLoadingData;
    }

    public static void setLastSavingData(List<Family> savingData) {
        AppController.lastSavingData = savingData;
    }

    public static List<Family> getLastSavingData() {
        return AppController.lastSavingData;
    }

    public static boolean equalsLoadAndSaveData() {
        return AppController.lastLoadingData != null
                && AppController.lastLoadingData.equals(AppController.lastSavingData);
    }
}
