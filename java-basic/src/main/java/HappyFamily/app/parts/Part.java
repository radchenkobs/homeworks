package HappyFamily.app.parts;

import HappyFamily.family.Family;
import HappyFamily.family.FamilyController;
import HappyFamily.helpers.Errors;
import HappyFamily.helpers.UserInMessage;
import libs.Console;
import libs.Input;
import libs.Parser;

import java.util.List;
import java.util.function.Function;

public abstract class Part implements IPart {
    private final String title;
    public final FamilyController controller;
    private boolean disabled = false;
    public Function<Part, Boolean> validator = null;

    public Part(String title, FamilyController controller, Function<Part, Boolean> validator) {
        this.title = title;
        this.controller = controller;
        this.validator = validator;
        init();
    }

    public Part(String title, FamilyController controller) {
        this(title, controller, null);
    }

    public void init() {
        if (validator != null) {
            validator.apply(this);
        }
    }

    @Override
    public boolean isDisabled() {
        return this.disabled;
    }

    public void setIsDisabled(boolean set) {
        this.disabled = set;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void displayTotal() {
        Console.print(String.format("Total: %s\n", controller.size()));
    }

    public void displayTotal(int num) {
        Console.print(String.format("Total: %s\n", num));
    }

    @Override
    public void displayFamilyList(List<Family> familyList) {
        for (int i = 0; i < familyList.size(); i++) {
            Family f = familyList.get(i);
            Console.title(String.format("#%d. ID:%d. _id:%d. uniqueId:%s.\n", i + 1, f.id, f._id, f.uniqueId));
            Console.print(f.prettyFormat());
        }
    }

    @Override
    public int inputFamilyCountNumber() {
        UserInMessage.enterFamilyCountNumber();
        String str = Input.string().trim();
        try {
            int num = Parser.parseIntFromString(str);
            if (num <= 0) {
                Errors.inputZeroNumber();
                return inputFamilyCountNumber();
            }
            return num;
        } catch (NumberFormatException ex) {
            Errors.inputParseIntException(str);
            return inputFamilyCountNumber();
        }
    }

    @Override
    public int inputNumber(String message) {
        UserInMessage.enterNumberMessage(message);
        String str = Input.string().trim();
        try {
            int num = Parser.parseIntFromString(str);
            if (num < 0) {
                Errors.inputLessZeroNumber();
                return inputNumber(message);
            }
            return num;
        } catch (NumberFormatException ex) {
            Errors.inputParseIntException(str);
            return inputNumber(message);
        }
    }

    @Override
    public IPart instance() {
        return PartProxy.newInstance(this);
    }

    public abstract void run();
}
