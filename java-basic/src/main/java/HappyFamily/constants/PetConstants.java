package HappyFamily.constants;

public class PetConstants {
    public static final int PET_MIN_TRICK_LEVEL = 1;
    public static final int PET_MAX_TRICK_LEVEL = 100;

    public static final int PET_MIN_AGE_LEVEL = 0;
    public static final int PET_MAX_AGE_LEVEL = 100;
}
