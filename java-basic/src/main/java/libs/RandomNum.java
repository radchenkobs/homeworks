package libs;

import java.util.Random;

public class RandomNum {

    public static boolean randomSwitch() {
        return Math.random() > 0.5;
    }

    public static int random(int min, int max) {
        Random random = new Random();
        return random.nextInt(min, max);
    }
}
