package HappyFamily.helpers;

import libs.Console;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public abstract class ClassInitializer {

    public static int classCounter = 0;
    public static int familyCounter = 0;
    public static int humanCounter = 0;
    public static int petCounter = 0;

    public String uniqueId;
    public int id;
    public int _id;
    private static boolean showInfo = false;

    {
        id = classCounter++;
        uniqueId = UUID.randomUUID().toString();
        String className = this.getClass().getSimpleName();
        if (showInfo) {
            Console.warning(String.format("Instance initializer \"%s\" \n", className));
        }
        if (className.equals("Family")) {
            _id = familyCounter++;
        }
        if (className.equals("Human")) {
            _id = humanCounter++;
        }
        if (className.equals("Pet")) {
            _id = petCounter++;
        }
    }

    static {
        if (showInfo) {
            Set<String> classNames = new HashSet<>();
            Arrays.stream(Thread.currentThread().getStackTrace()).forEach(element -> {
                String name = element.getClassName();
                if (!name.contains("java")) {
                    classNames.add(name);
                }
                classNames.add(name);
            });
            for (String className : classNames) {
                Console.warning(String.format("Class initializer \"%s\" \n", className));
            }
        }
    }

    public static void disable() {
        showInfo = false;
    }

    public static void enable() {
        showInfo = true;
    }

    public void setUId(String uniqueId) {
        this.uniqueId = uniqueId;
    }
}
