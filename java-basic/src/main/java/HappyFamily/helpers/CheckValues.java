package HappyFamily.helpers;

import HappyFamily.enums.Messages;

import static HappyFamily.constants.HumanConstants.HUMAN_MAX_IQ;
import static HappyFamily.constants.PetConstants.*;
import static HappyFamily.constants.HumanConstants.*;

public class CheckValues {
    public static int petTrickLevel(int trickLevel) {
        if (trickLevel < PET_MIN_TRICK_LEVEL || trickLevel > PET_MAX_TRICK_LEVEL) {
            Errors.runtimeException(Messages.INVALID_PET_TRICK_LEVEL);
        }
        return trickLevel;
    }

    public static int petAge(int trickLevel) {
        if (trickLevel < PET_MIN_AGE_LEVEL || trickLevel > PET_MAX_AGE_LEVEL) {
            Errors.runtimeException(Messages.INVALID_PET_AGE);
        }
        return trickLevel;
    }

    public static String emptyString(String str) {
        if (str == null || str.length() == 0) {
            Errors.runtimeException(Messages.EMPTY_STRING);
        }
        return str;
    }

    public static int humanYear(int year) {
        if (year < HUMAN_MIN_YEAR || year > HUMAN_MAX_YEAR) {
            Errors.runtimeException(Messages.INVALID_HUMAN_YEAR);
        }
        return year;
    }

    public static int humanIQ(int iq) {
        if (iq < HUMAN_MIN_IQ || iq > HUMAN_MAX_IQ) {
            Errors.runtimeException(Messages.INVALID_HUMAN_IQ);
        }
        return iq;
    }
}
