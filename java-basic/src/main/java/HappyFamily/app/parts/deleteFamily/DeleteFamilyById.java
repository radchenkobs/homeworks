package HappyFamily.app.parts.deleteFamily;

import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.InsertTestData;
import HappyFamily.app.parts.Part;
import HappyFamily.family.FamilyController;
import libs.Console;
import libs.Logger;

public class DeleteFamilyById extends Part {
    public DeleteFamilyById(FamilyController controller) {
        super("Delete family by id", controller);
    }

    public static IPart of(FamilyController controller) {
        return new DeleteFamilyById(controller).instance();
    }

    @Override
    public void run() {
        int num = inputNumber("Enter family id");
        Logger.input(String.format("%d", num));
        boolean status = controller.deleteFamilyById(num);
        String msg = status ? String.format("Family with id %d has been removed", num) : "Family was not deleted";
        Console.print(msg);
    }
}
