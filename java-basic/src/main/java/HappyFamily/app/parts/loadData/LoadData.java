package HappyFamily.app.parts.loadData;

import HappyFamily.app.AppController;
import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.Part;
import HappyFamily.family.Family;
import HappyFamily.family.FamilyController;
import libs.Console;
import libs.FamiliesFiles;
import libs.Logger;

import java.io.IOException;
import java.util.List;

public class LoadData extends Part {

    public LoadData(FamilyController controller) {
        super("Load data", controller, o -> AppController.getLastLoadingData() == null);
    }

    public static IPart of(FamilyController controller) {
        return new LoadData(controller).instance();
    }

    @Override
    public void run() {
        try {
            List<Family> familyList = FamiliesFiles.read();
            AppController.setLastLoadingData(familyList);
            controller.loadData(familyList);
        } catch (RuntimeException ex) {
            String msg = String.format("Read data from file error: %s", ex.getMessage());
            Logger.error(msg);
            Console.error(msg + "\n");
        }
    }
}
