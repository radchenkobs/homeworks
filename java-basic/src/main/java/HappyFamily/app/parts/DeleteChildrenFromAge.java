package HappyFamily.app.parts;

import HappyFamily.family.FamilyController;
import libs.Console;
import libs.Logger;

public class DeleteChildrenFromAge extends Part {

    public DeleteChildrenFromAge(FamilyController controller) {
        super("Delete children from families older than age", controller, o -> !o.controller.isEmpty());
    }

    public static IPart of(FamilyController controller) {
        return new DeleteChildrenFromAge(controller).instance();
    }

    @Override
    public void run() {
        int num = inputNumber("Enter age");
        Logger.input(String.format("%d", num));
        int deletedChildrenCount = controller.deleteAllChildrenOlderThen(num);
        Console.print(String.format("Deleted %d\n", deletedChildrenCount));
    }
}
