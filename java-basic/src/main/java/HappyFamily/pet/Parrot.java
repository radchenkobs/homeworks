package HappyFamily.pet;

import HappyFamily.enums.Species;
import HappyFamily.interfaces.IPetCanFoul;
import libs.Console;

import java.util.Set;

public class Parrot extends Pet implements IPetCanFoul {

    public Parrot(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, Species.PARROT, age, trickLevel, habits);
    }

    public Parrot(String nickname) {
        this(nickname, 0, 30, null);
    }

    @Override
    public String respond() {
        msg();
        String msg = String.format("Hi, I am %s!\n", getNickname());
        Console.print(msg);
        return msg;
    }

    @Override
    public boolean foul() {
        msg();
        Console.print("I need to cover up my tracks...\n");
        return true;
    }
}
