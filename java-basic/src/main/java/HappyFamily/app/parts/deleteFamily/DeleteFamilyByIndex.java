package HappyFamily.app.parts.deleteFamily;

import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.Part;
import HappyFamily.family.FamilyController;
import libs.Console;
import libs.Logger;

public class DeleteFamilyByIndex extends Part {
    public DeleteFamilyByIndex(FamilyController controller) {
        super("Delete family by index", controller);
    }

    public static IPart of(FamilyController controller) {
        return new DeleteFamilyByIndex(controller).instance();
    }

    @Override
    public void run() {
        int num = inputNumber("Enter family index");
        Logger.input(String.format("%d", num));
        boolean status = controller.deleteFamilyByIndex(num - 1);
        String msg = status ? String.format("Family with index %d has been removed", num) : "Family was not deleted";
        Console.print(msg);
    }
}
