package HappyFamily.app.parts.editFamily.adoptChild;

import HappyFamily.app.stepper.Step;
import HappyFamily.app.stepper.StepRadio;
import HappyFamily.app.stepper.Stepper;
import HappyFamily.constants.HumanConstants;
import HappyFamily.family.FamilyController;
import libs.DateUtil;
import libs.Parser;

import java.util.Arrays;
import java.util.List;

public class AdoptChildStepper {
    private final Stepper steps = new Stepper();

    public record AdoptChildData(Integer index, boolean isMale, String name, String surname, String birthday,
                                 Integer iq) {
    }

    public static class ParserFormatBirthDay {

        private final DateUtil date = DateUtil.of();

        private final String value;

        public ParserFormatBirthDay(String value) {
            this.value = value;
        }

        private int parseYear(String str) {
            int y = Parser.parseIntFromString(str);

            if (y < HumanConstants.HUMAN_MIN_YEAR) {
                throw new RuntimeException(String.format("Year: %s must be bigger or equals than %s.", y, HumanConstants.HUMAN_MIN_YEAR));
            }

            if (y > HumanConstants.HUMAN_MAX_YEAR) {
                throw new RuntimeException(String.format("Year: %s must be less or equals than %s.", y, HumanConstants.HUMAN_MAX_YEAR));
            }

            return y;
        }

        private int parseMonth(int year, String month) {
            int m = Parser.parseIntFromString(month);
            int maxMonth = date.getYear() == year ? date.getMonth() : 12;

            if (m < 1) {
                throw new RuntimeException("Month: %s must be bigger or equals than 1.");
            }

            if (m > maxMonth) {
                throw new RuntimeException(String.format("Month: %s must be less or equals than %s.", m, maxMonth));
            }

            return m;
        }

        private int parseDay(int year, int month, String day) {
            int d = Parser.parseIntFromString(day);
            int maxDayOfLength = date.getYear() == year && date.getMonth() == month ? date.getDayOfMonth() : DateUtil.lengthOfMonth(year, month);

            if (d < 1) {
                throw new RuntimeException("Day: %s must be bigger or equals than 1.");
            }

            if (d > maxDayOfLength) {
                throw new RuntimeException(String.format("Day: %s must be less or equals than %s.", d, maxDayOfLength));
            }

            return d;
        }

        public String parser() {
            String separatorPattern = "[/.-_ ]{1}";
            String pattern = String.format("\\d{1,2}%s\\d{1,2}%s\\d{4}", separatorPattern, separatorPattern);
            String str = Parser.parseRegexFormat(value, pattern);
            List<String> splits = Arrays.stream(str
                            .replace("/", " ")
                            .replace(".", " ")
                            .replace("-", " ")
                            .split(" "))
                    .map(String::trim)
                    .toList();
            int year = parseYear(splits.get(2));
            int month = parseMonth(year, splits.get(1));
            parseDay(year, month, splits.get(0));
            return value;
        }
    }

    public AdoptChildStepper(FamilyController controller) {
        steps.add(new Step<Integer>("Enter family index:", s -> Parser.parseIntFromString(s, 1, controller.size())));
        steps.add(new StepRadio("Is male?"));
        steps.add(new Step<>("Enter name"));
        steps.add(new Step<>("Enter surname"));
        steps.add(new Step<>("Enter birthday", s -> new ParserFormatBirthDay(s).parser()));
        steps.add(new Step<Integer>("Enter iq", s -> Parser.parseIntFromString(s, HumanConstants.HUMAN_MIN_IQ, HumanConstants.HUMAN_MAX_IQ)));
        steps.run();
    }

    public AdoptChildData getData() {
        Integer index = (Integer) steps.get(0).getValidateValue();
        Boolean isMale = (Boolean) steps.get(1).getValidateValue();
        String name = steps.get(2).getValue();
        String surname = steps.get(3).getValue();
        String birthday = steps.get(4).getValue();
        Integer iq = (Integer) steps.get(5).getValidateValue();
        return new AdoptChildData(index, isMale, name, surname, birthday, iq);
    }
}
