package HappyFamily.app.parts.editFamily.bornChild;

import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.Part;
import HappyFamily.family.FamilyController;
import HappyFamily.humans.Human;
import libs.Console;
import libs.Logger;

public class BornChild extends Part {
    public BornChild(FamilyController controller) {
        super("Born Child", controller);
    }

    public static IPart of(FamilyController controller) {
        return new BornChild(controller).instance();
    }

    @Override
    public void run() {
        BornChildStepper bornChildStepper = new BornChildStepper(controller);
        int index = bornChildStepper.getData().index();
        String maleName = bornChildStepper.getData().maleName();
        String femaleName = bornChildStepper.getData().femaleName();
        Human child = controller.bornChild(index, maleName, femaleName);
        if (child != null) {
            String msg = String.format("Child: %s", child.prettyFormat());
            Console.print(msg + "\n");
            Logger.info(String.format("A child is born. %s", msg));
        }
    }
}
