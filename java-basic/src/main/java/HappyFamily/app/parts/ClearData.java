package HappyFamily.app.parts;

import HappyFamily.app.AppController;
import HappyFamily.family.FamilyController;
import libs.Console;
import libs.Logger;

import java.util.List;

public class ClearData extends Part {
    public ClearData(FamilyController controller) {
        super("Clear data", controller, o -> !o.controller.isEmpty());
    }

    public static IPart of(FamilyController controller) {
        return new ClearData(controller).instance();
    }

    @Override
    public void run() {
        List<Integer> idToRemove = controller.getAllFamilies().stream().map(f -> f.id).toList();
        idToRemove.forEach(controller::deleteFamilyById);
        AppController.setLastSavingData(null);
        AppController.setLastLoadingData(null);
        String msg = String.format("Clear data. Deleted %d elements.", idToRemove.size());
        Logger.info(msg);
        Console.print(msg);
    }
}
