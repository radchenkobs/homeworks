import HappyFamily.family.Family;
import HappyFamily.enums.DayOfWeek;
import HappyFamily.Schedule;
import HappyFamily.humans.Human;
import HappyFamily.humans.Man;
import HappyFamily.humans.Woman;
import HappyFamily.pet.Cat;
import HappyFamily.pet.Dog;

import libs.DateUtil;
import org.junit.Test;
import org.junit.Before;

import java.util.List;

import static org.junit.Assert.*;

public class FamilyTest {
    private Woman daughter;
    private Man son;
    private Woman mother;
    private Man father;
    private Family family;
    private Dog dog;
    private Cat cat;

    @Before
    public void starter() {
        Schedule schedule = new Schedule();
        for (DayOfWeek d : DayOfWeek.values()) {
            schedule.setTask(d, String.format("Task_%s_1", d));
        }

        daughter = new Woman(
                "Mary",
                "Karleone",
                DateUtil.of(1900, 3, 2).getTimestamp(),
                null,
                100,
                null
        );

        son = new Man(
                "Michael",
                "Karleone",
                DateUtil.of(2000, 4, 3).getTimestamp(),
                null,
                0,
                schedule
        );

        mother = new Woman(
                "Jane",
                "Karleone",
                DateUtil.of(1979, 10, 12).getTimestamp(),
                null,
                140,
                null
        );
        father = new Man(
                "Vito",
                "Karleone",
                DateUtil.of(1977, 11, 22).getTimestamp(),
                null,
                120,
                null
        );

        dog = new Dog("Doggy");
        cat = new Cat("Cat");
        family = new Family(mother, father);
    }

    @Test
    public void deleteChildByInstantsTest() {
        family.addChild(son);
        family.addChild(daughter);

        List<Human> childrenBeforeDelete = family.getChildren();
        boolean checkHasChildBefore = family.hasChild(son);

        boolean state = family.deleteChild(son);

        List<Human> childrenAfterDelete = family.getChildren();
        boolean checkHasChildAfter = family.hasChild(son);

        assertEquals("Family delete by instants test method return state", state, (checkHasChildBefore && !checkHasChildAfter));
        assertEquals("Family delete by instants test check deleted child family", null, son.getFamily());
    }

    @Test
    public void deleteChildByIndexTest() {
        family.addChild(son);
        family.addChild(daughter);

        int childrenBeforeDeleteLength = family.getChildren().size();
        boolean checkHasChildBefore = family.hasChild(son);

        boolean state = family.deleteChild(0);

        int childrenAfterDeleteLength = family.getChildren().size();
        boolean checkHasChildAfter = family.hasChild(son);

        assertEquals("Family delete by idx test method return state", state, (checkHasChildBefore && !checkHasChildAfter));
        assertEquals("Family delete by idx test check length", childrenBeforeDeleteLength, childrenAfterDeleteLength + 1);
    }

    @Test
    public void addChild() {
        family.addChild(daughter);
        List<Human> resOne = family.getChildren();
        int expOne = 1;
        assertEquals("Family add child test one", expOne, resOne.size());

        boolean checkHasChildBefore = family.hasChild(son);
        family.addChild(son);
        boolean checkHasChildAfter = family.hasChild(son);
        List<Human> resTwo = family.getChildren();
        int expTwo = expOne + 1;
        assertEquals("Family add child test two", expTwo, resTwo.size());
        assertTrue("Family add child test object", checkHasChildBefore != checkHasChildAfter);
    }

    @Test
    public void countTest() {
        int resOne = family.countFamily();
        int expOne = 2;
        assertEquals("Family count test one", expOne, resOne);

        family.addPet(dog);
        family.addPet(cat);
        int resTwo = family.countFamily();
        int expTwo = 4;
        assertEquals("Family count test two", expTwo, resTwo);
    }

    @Test
    public void toStringTest() {
        String msg = family.toString();
        String part1 = "Mother: Female {name='Jane', surname='Karleone', birthday=12/10/1979, iq=140}, schedule=none};\n";
        String part2 = "Father: Male   {name='Vito', surname='Karleone', birthday=22/11/1977, iq=120}, schedule=none};\n";
        String part3 = "Children (0): []\n";
        String expectedEmpty = part1 + part2 + part3;
        assertEquals("toString object empty children state", expectedEmpty, msg);

        family.addChild(daughter);
        msg = family.toString();
        part3 = "Children (1): [Female {name='Mary', surname='Karleone', birthday=02/03/1900, iq=100}, schedule=none}]\n";
        String expected = part1 + part2 + part3;
        assertEquals("toString object empty children state", expected, msg);
    }

    @Test
    public void equalsTest() {
        Family f1 = family;
        boolean eqOne = family.equals(f1);
        assertTrue("Family equals test one", eqOne);

        f1 = null;
        boolean eqTwo = family.equals(f1);
        assertFalse("Family equals test one", eqTwo);
    }
}
