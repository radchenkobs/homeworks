package HappyFamily.app.parts.editFamily.bornChild;

import HappyFamily.app.stepper.Step;
import HappyFamily.app.stepper.Stepper;
import HappyFamily.family.FamilyController;
import libs.Parser;

public class BornChildStepper {
    private final Stepper steps = new Stepper();

    public record BornChildData(Integer index, String maleName, String femaleName) {
    }

    public BornChildStepper(FamilyController controller) {
        steps.add(new Step<Integer>("Enter family index", s -> Parser.parseIntFromString(s, 0, controller.size())));
        steps.add(new Step<>("Enter male name"));
        steps.add(new Step<>("Enter female name"));
        steps.run();
    }

    public BornChildData getData() {
        Integer index = (Integer) steps.get(0).getValidateValue();
        String maleName = steps.get(1).getValue();
        String femaleName = steps.get(1).getValue();
        return new BornChildData(index, maleName, femaleName);
    }
}
