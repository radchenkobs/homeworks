package HappyFamily.family;

import HappyFamily.humans.Human;
import HappyFamily.humans.Man;
import HappyFamily.interfaces.IFamilyDao;
import HappyFamily.pet.Pet;
import libs.Console;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyService {

    private final int MAX_CHILDREN_COUNT = 3;
    private final IFamilyDao db = new CollectionFamilyDao();

    public void replaceData(List<Family> familyList) {
        db.replaceData(familyList);
    }

    public void loadData(List<Family> familyList) {
        db.loadData(familyList);
    }

    public List<Family> getAllFamilies() {
        return db.getAllFamilies();
    }

    public void displayAllFamilies() {
        Console.print(db.getAllFamilies().toString());
    }

    public List<Family> getFamiliesBiggerThan(int count) throws FamilyOverflowException {
        if (count < 0) {
            throw new FamilyOverflowException("GET: (getFamiliesBiggerThan) Family count must be bigger then 0");
        }
        return db.getAllFamilies().stream().filter(f -> f.countFamily() >= count).toList();
    }

    public List<Family> getFamiliesLessThan(int count) throws FamilyOverflowException {
        if (count < 0) {
            throw new FamilyOverflowException("GET: (getFamiliesLessThan) Family count must be bigger then 0");
        }
        return db.getAllFamilies().stream().filter(f -> f.countFamily() <= count).toList();
    }

    public int countFamiliesWithMemberNumber(int count) throws FamilyOverflowException {
        if (count < 0) {
            throw new FamilyOverflowException("GET: (countFamiliesWithMemberNumber) Family count must be bigger then 0");
        }
        return db.getAllFamilies().stream().filter(f -> f.countFamily() == count).toList().size();
    }

    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        db.saveFamily(family);
        return family;
    }

    public Family createNewFamily(Family family) {
        db.saveFamily(family);
        return family;
    }

    public boolean deleteFamilyByIndex(int index) throws FamilyOverflowException {
        if (index < 0 || index > db.getAllFamilies().size()) {
            throw new FamilyOverflowException("DELETE: (deleteFamilyByIndex) Family count must be bigger then 0");
        }
        return db.deleteFamily(index);
    }

    public boolean deleteFamilyById(int id) throws FamilyOverflowException {
        if (id < 0) {
            throw new FamilyOverflowException("DELETE: (deleteFamilyById) Family id must be positive");
        }
        return db.deleteFamilyById(id);
    }

    public Human bornChild(Family family, String mName, String fName) throws FamilyOverflowException {
        if (family.getChildren().size() > MAX_CHILDREN_COUNT) {
            throw new FamilyOverflowException(String.format("PUT: (bornChild by family) Family children max length; Max count: %d", MAX_CHILDREN_COUNT));
        }
        Human child = family.getMother().bornChild();
        if (child instanceof Man) {
            child.setName(mName);
        } else {
            child.setName(fName);
        }
        db.saveFamily(family);
        return child;
    }

    public Human bornChild(int index, String mName, String fName) throws FamilyOverflowException {
        if (index < 0 || index >= count()) {
            throw new FamilyOverflowException("PUT: (bornChild by index) Invalid family index");
        }
        Family family = getFamilyByIndex(index);
        if (family.getChildren().size() > MAX_CHILDREN_COUNT) {
            throw new FamilyOverflowException(String.format("PUT: (bornChild by index) Family children max length; Max count: %d", MAX_CHILDREN_COUNT));
        }
        return bornChild(family, mName, fName);
    }

    public Human bornChildById(int id, String mName, String fName) throws FamilyOverflowException {
        if (id < 0) {
            throw new FamilyOverflowException("PUT: (bornChildById) Invalid family id");
        }
        Family family = getFamilyById(id);
        if (family.getChildren().size() > MAX_CHILDREN_COUNT) {
            throw new FamilyOverflowException(String.format("PUT: (bornChild by index) Family children max length; Max count: %d", MAX_CHILDREN_COUNT));
        }
        return bornChild(family, mName, fName);
    }

    public void adoptChild(Family family, Human child) throws FamilyOverflowException {
        if (family.getChildren().size() > MAX_CHILDREN_COUNT) {
            throw new FamilyOverflowException(String.format("PUT: (adoptChild) Family children max length; Max count: %d", MAX_CHILDREN_COUNT));
        }
        family.addChild(child);
        db.saveFamily(family);
    }

    public void adoptChild(int index, Human child) throws FamilyOverflowException {
        Family family = db.getFamilyByIndex(index);
        if (family.getChildren().size() > MAX_CHILDREN_COUNT) {
            throw new FamilyOverflowException(String.format("PUT: (adoptChild) Family children max length; Max count: %d", MAX_CHILDREN_COUNT));
        }
        family.addChild(child);
        db.saveFamily(family);
    }

    public List<Human> deleteAllChildrenOlderThen(int age) throws FamilyOverflowException {
        if (age < 0) {
            throw new FamilyOverflowException("DELETE: (deleteAllChildrenOlderThen) Invalid age");
        }
        List<Human> deletedHuman = new ArrayList<>();
        db.getAllFamilies()
                .stream()
                .filter(f -> f.getChildren().size() > 0 && f.maxChildrenAge() >= age)
                .forEach(family -> {
                    List<Human> childrenToDelete = family
                            .getChildren()
                            .stream()
                            .filter(c -> c.getAge() >= age)
//                            .forEach(family::deleteChild) ??
                            .toList();
                    for (Human child : childrenToDelete) {
                        family.deleteChild(child);
                        deletedHuman.add(child);
                    }
                    db.saveFamily(family);
                });
        return deletedHuman;
    }

    public int count() {
        return db.count();
    }

    public Family getFamilyByIndex(int index) throws FamilyOverflowException {
        if (index < 0 || index >= count()) {
            throw new FamilyOverflowException("GET: (getFamilyByIndex) Index out of bound");
        }
        return db.getFamilyByIndex(index);
    }

    public Family getFamilyById(int id) throws FamilyOverflowException {
        if (id < 0) {
            throw new FamilyOverflowException("GET: (getFamilyById) Id out of bound");
        }
        return db.getFamilyById(id);
    }

    public Set<Pet> getPets(int index) {
        return db.getFamilyByIndex(index).getPets();
    }

    public boolean addPet(int index, Pet pet) {
        Family f = db.getFamilyByIndex(index);
        f.addPet(pet);
        db.saveFamily(f);
        return true;
    }
}
