package HappyFamily.enums;

import libs.WorkerString;

public enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    public final String name;

    public Formatter format;

    public static class Formatter {
        public final String capitalize;

        public Formatter(DayOfWeek day) {
            this.capitalize = WorkerString.capitalize(day.name);
        }

    }

    DayOfWeek() {
        this.name = this.name();
        this.format = new Formatter(this);
    }
}
