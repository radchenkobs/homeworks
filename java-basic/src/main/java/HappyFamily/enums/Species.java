package HappyFamily.enums;

public enum Species {
    CAT("Cat", false, 4, true),
    DOG("Dog", false, 4, true),
    FISH("Fish", false, 0, false),
    PARROT("Parrot", true, 2, false),
    UNKNOWN("UNKNOWN", null, null, null);

    public final String name = this.name();

    public final String value;
    public final Boolean canFly;
    public final Integer numberOfLegs;
    public final Boolean hasFur;

    Species(String value, Boolean canFly, Integer numberOfLegs, Boolean hasFur) {
        this.value = value;
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
}
