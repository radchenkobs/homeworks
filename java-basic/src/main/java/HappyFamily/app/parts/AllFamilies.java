package HappyFamily.app.parts;

import HappyFamily.family.Family;
import HappyFamily.family.FamilyController;
import libs.Console;

import java.util.List;

public class AllFamilies extends Part {

    public AllFamilies(FamilyController controller) {
        super("Show all list of families", controller, o -> !o.controller.isEmpty());
    }

    public static IPart of(FamilyController controller) {
        return new AllFamilies(controller).instance();
    }

    @Override
    public void run() {
        List<Family> familyList = controller.getAllFamilies();
        displayTotal(familyList.size());
        displayFamilyList(familyList);
        if (familyList.size() > 2) {
            displayTotal(familyList.size());
        }
    }
}
