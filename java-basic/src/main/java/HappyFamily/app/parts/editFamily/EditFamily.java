package HappyFamily.app.parts.editFamily;

import HappyFamily.app.menu.EditFamilyMenu;
import HappyFamily.app.menu.Menu;
import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.Part;
import HappyFamily.family.FamilyController;

public class EditFamily extends Part {

    private final Menu menu = new EditFamilyMenu(controller);

    public EditFamily(FamilyController controller) {
        super("Edit family", controller, o -> !o.controller.isEmpty());
    }

    public static IPart of(FamilyController controller) {
        return new EditFamily(controller).instance();
    }

    @Override
    public void run() {
        menu.run();
    }
}
