package HappyFamily.family;

import HappyFamily.enums.HumanSex;
import HappyFamily.helpers.ClassInitializer;
import HappyFamily.humans.Human;
import HappyFamily.humans.Man;
import HappyFamily.humans.Woman;
import HappyFamily.pet.Pet;
import libs.Console;

import java.util.*;
import java.util.stream.Collectors;

public class Family extends ClassInitializer {
    private Woman mother;
    private Man father;
    private List<Human> children = new ArrayList<Human>();
    private Set<Pet> pets = new HashSet<Pet>();

    public <H extends Human> Family(H mother, H father) {
        this.mother = (Woman) mother;
        this.father = (Man) father;
        mother.setFamily(this);
        father.setFamily(this);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        Console.warning(String.format("Instance \"%s\" hashCode: %d REMOVED\n", this.getClass().getSimpleName(), hashCode()));
    }

    @Override
    public String toString() {
        String childrenString = children == null ? "none" : children.toString();
        int childrenCount = children == null ? 0 : children.size();
        return String.format("Mother: %s;\nFather: %s;\nChildren (%d): %s\n", mother, father, childrenCount, childrenString);
    }

    public String prettyFormat() {
        String childrenCounter = children == null ? "" : String.valueOf(children.size());
        String childrenTitleString = String.format("\tchildren: %s\n", childrenCounter);
        String childrenDescriptionString = children == null ? "" : children
                .stream()
                .map(child -> {
                    String sex = child.getSex() == HumanSex.MALE ? "boy" : "girl";
                    return String.format("\t\t%s: %s,\n", sex, child.prettyFormat());
                }).collect(Collectors.joining());
        String childrenString = String.format("%s%s", childrenTitleString, childrenDescriptionString);

        String petsTitleString = getPets() == null ? "none" : String.valueOf(getPets().size());
        String petsDescriptionString = getPets() == null ? "" : getPets()
                .stream()
                .map(p -> String.format("\t\t%s", p.prettyFormat()))
                .collect(Collectors.joining());

        String petsString = String.format("\tpets: %s\n %s", petsTitleString, petsDescriptionString);
        return String.format("family: (%d)\n \tmother: %s,\n \tfather: %s,\n %s %s\r\n", countFamily(), mother.prettyFormat(), father.prettyFormat(), childrenString, petsString);
    }

    @Override
    public int hashCode() {
        int code = 0;
        if (mother != null) code += mother.hashCode();
        if (father != null) code += father.hashCode();
        if (children != null) {
            for (Human child : children) {
                if (child == null) continue;
                code += child.hashCode();
            }
        }
        if (pets != null) {
            for (Pet pet : pets) {
                if (pet == null) continue;
                code += pet.hashCode();
            }
        }

        return code;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Family that = (Family) obj;
        return that.mother.equals(this.mother)
                && that.father.equals(this.father)
                && this.children.equals(that.children)
                && this.pets.equals(that.pets);
    }

    public Woman getMother() {
        return this.mother;
    }

    public Man getFather() {
        return this.father;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
    }

    public List<Human> getChildren() {
        return this.children;
    }

    public void showChildren() {
        if (children == null) {
            Console.print(String.format("No children in this family:\n %s", this));
            return;
        }
        Console.print(String.format("Children - %d\n", children.size()));
        for (Human child : this.children) {
            Console.print(String.format("%s\n", child));
        }
        Console.print("\n");
    }

    public boolean hasChild(Human child) {
        if (child == null || children == null) return false;
        boolean familyHasChild = false;
        for (Human c : children) {
            if (c.equals(child)) {
                familyHasChild = true;
                break;
            }
        }
        return familyHasChild;
    }

    public boolean addChild(Human child) {
        child.setFamily(this);
        return children.add(child);
    }

    public boolean deleteChild(Human child) {
        if (child == null || children == null) return false;
        child.setFamily(null);
        return children.remove(child);
    }

    public boolean deleteChild(int idx) {
        if (children == null || idx < 0 || idx >= children.size()) return false;
        Human removedChild = children.remove(idx);
        removedChild.setFamily(null);
        return true;
    }

    public int countFamily() {
        int count = 0;
        if (mother != null) count++;
        if (father != null) count++;
        if (children != null) {
            for (Human child : children) {
                if (child == null) continue;
                count++;
            }
        }
        if (pets != null) {
            for (Pet pet : pets) {
                if (pet == null) continue;
                count++;
            }
        }
        return count;
    }

    public Integer maxChildrenAge() {
        if (children.size() == 0) return null;
        return children.stream().map(Human::getAge).reduce(0, Integer::max);
    }
}
