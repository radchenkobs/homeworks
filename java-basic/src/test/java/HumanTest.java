import HappyFamily.constants.Names;
import HappyFamily.family.Family;
import HappyFamily.Schedule;
import HappyFamily.enums.DayOfWeek;
import HappyFamily.enums.Messages;
import HappyFamily.humans.Human;
import HappyFamily.humans.Man;
import HappyFamily.humans.Woman;
import libs.DateUtil;
import libs.RandomNum;
import org.junit.Test;

import static org.junit.Assert.*;

public class HumanTest {

    public Man createRandomMan() {
        int year = RandomNum.random(1970, 1980);
        int month = RandomNum.random(1, 12);
        int day = RandomNum.random(1, 28);
        long timestamp = DateUtil.of(year, month, day).getTimestamp();
        int iq = RandomNum.random(100, 180);
        String name = Names.getRandomMaleName();
        return new Man(
                name,
                "Karleone",
                timestamp,
                null,
                iq,
                null
        );
    }

    public Woman createRandomWoman() {
        int year = RandomNum.random(1970, 1980);
        int month = RandomNum.random(1, 12);
        int day = RandomNum.random(1, 28);
        long timestamp = DateUtil.of(year, month, day).getTimestamp();
        int iq = RandomNum.random(100, 180);
        String name = Names.getRandomFemaleName();
        return new Woman(
                name,
                "Karleone",
                timestamp,
                null,
                iq,
                null
        );
    }

    public Man createMan() {
        return new Man(
                "Vito",
                "Karleone",
                DateUtil.of(1977, 10, 10).getTimestamp(),
                null,
                130,
                null
        );
    }

    public Woman createWoman() {
        return new Woman(
                "Jane",
                "Karleone",
                DateUtil.of(1979, 3, 2).getTimestamp(),
                null,
                140,
                null
        );
    }


    @Test
    public void createFromBirthdayStringTest() {
        Woman woman = new Woman(
                "Jane",
                "Karleone",
                "20/03/2016",
                140
        );
        String msg = woman.getBirthDate();
        String expected = "20/03/2016";
        assertEquals("Human create with string birthday test", expected, msg);
    }

    @Test
    public void toStringTest() {
        Man man = createMan();
        String msg = man.toString();
        String expected = "Male   {name='Vito', surname='Karleone', birthday=10/10/1977, iq=130}, schedule=none}";
        assertEquals("toString empty object state", expected, msg);
    }

    @Test
    public void equalsTest() {
        Man man = createMan();
        Woman woman = createWoman();
        boolean eqOne = woman.equals(man);
        assertFalse("Human equals test one", eqOne);

        Human human = man;
        boolean eqTwo = human.equals(man);
        assertTrue("Human equals test two", eqTwo);

        boolean eqThree = man.equals(man);
        assertTrue("Human equals test three", eqThree);
    }

    @Test
    public void scheduleTest() {
        Man man = new Man(
                "Vito",
                "Karleone",
                1977,
                null,
                120,
                null
        );
        Schedule schedule = new Schedule();
        schedule.setTask(DayOfWeek.MONDAY, String.format("Task_%s_1", DayOfWeek.MONDAY));
        man.setSchedule(schedule);
        String msg = man.getSchedule().toString();
        String expected = schedule.toString();
        assertEquals("Human schedule test", expected, msg);
    }

    @Test
    public void bornChildWithoutFamilyTest() {
        Woman mother = new Woman(
                "Jane",
                "Karleone",
                1979,
                null,
                140,
                null
        );
        try {
            mother.bornChild();
        } catch (RuntimeException ex) {
            String res = ex.getMessage();
            String expMessage = Messages.OOPS.message;
            assertEquals("Human born child test without family", expMessage, res);
        }
    }

    @Test
    public void bornChildWithFamilyTest() {
        Woman mother = new Woman(
                "Jane",
                "Karleone",
                1979,
                null,
                140,
                null
        );
        Man father = new Man(
                "Vito",
                "Karleone",
                1977,
                null,
                120,
                null
        );

        Family family = new Family(mother, father);
        mother.bornChild();
        int childrenLengthAfter = family.getChildren().size();
        int expectedLengthAfter = 1;

        int expectedIq = (father.getIq() + mother.getIq()) / 2;

        assertEquals("Human test born child", childrenLengthAfter, expectedLengthAfter);
        assertEquals("Human test born child surname", family.getChildren().get(0).getSurname(), father.getSurname());
        assertEquals("Human test born child family", family.getChildren().get(0).getFamily(), family);
        assertEquals("Human test born child iq", family.getChildren().get(0).getIq(), expectedIq);
    }
}
