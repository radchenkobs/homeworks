package HappyFamily.app.menu;

import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.editFamily.adoptChild.AdoptChild;
import HappyFamily.app.parts.editFamily.bornChild.BornChild;
import HappyFamily.family.FamilyController;

import java.util.HashMap;
import java.util.Map;

public class EditFamilyMenu extends Menu {

    public EditFamilyMenu(FamilyController controller) {
        super("Edit family menu");

        Map<Integer, IPart> menu = new HashMap<>();

        menu.put(1, BornChild.of(controller));
        menu.put(2, AdoptChild.of(controller));

        setMenu(menu);
    }
}
