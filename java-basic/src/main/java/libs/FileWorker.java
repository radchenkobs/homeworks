package libs;

import java.io.*;

public class FileWorker {
    enum Path {
        LOG("application.log"),
        FAMILIES("families.json");

        public final String assets = "assets/";

        public final String name;

        Path(String name) {
            this.name = assets + name;
        }
    }

    private static boolean isFileExist(File file) {
        return file.exists();
    }

    private static void exists(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        if (!isFileExist(file)) {
            throw new FileNotFoundException(String.format("File %s is not exist", file.getName()));
        }
    }

    public static String read(String fileName) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();

        exists(fileName);

        File file = new File(fileName);

        try {
            try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
                String s;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        return sb.toString();
    }

    public static String read(Path path) throws FileNotFoundException {
        return read(path.name);
    }

    public static void write(String fileName, String text) throws RuntimeException {
        File file = new File(fileName);

        try {
            if (!isFileExist(file)) {
                file.createNewFile();
            }

            try (PrintWriter out = new PrintWriter(file.getAbsoluteFile())) {
                out.print(text);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void write(Path path, String text) throws RuntimeException {
        write(path.name, text);
    }

    public static void update(String fileName, String text) throws RuntimeException, FileNotFoundException {
        File file = new File(fileName);
        if (!isFileExist(file)) {
            write(fileName, text);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(read(fileName));
            sb.append(text);
            write(fileName, new String(sb));
        }
    }

    public static void update(Path path, String text) throws RuntimeException, FileNotFoundException {
        update(path.name, text);
    }

    public static void delete(String nameFile) throws FileNotFoundException, RuntimeException {
        exists(nameFile);
        boolean isDeleted = new File(nameFile).delete();
        if (!isDeleted) {
            throw new RuntimeException("The file was not deleted");
        }
    }

    public static void delete(Path path) throws FileNotFoundException, RuntimeException {
        delete(path.name);
    }

}
