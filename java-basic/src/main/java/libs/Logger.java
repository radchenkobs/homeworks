package libs;

import java.io.FileNotFoundException;
import java.util.*;

public final class Logger {

    public static boolean disabled = false;

    enum LogType {
        START,
        ERROR,
        INFO,
        DEBUG,
        WARNING,
        EXIT,
        INPUT;

        public final String name;

        LogType() {
            this.name = String.format("[%s]", this.name());
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    static class Log {
        private final long timestamp;
        private final LogType logType;
        private final String message;

        public Log(long timestamp, LogType logType, String message) {
            this.logType = logType;
            this.message = message;
            this.timestamp = timestamp;
        }

        @Override
        public String toString() {
            String formattedTime = DateUtil.of(timestamp).formatter("dd/MM/yyyy HH:mm:ss");
            return String.format("%s %-8s: %s\r\n", formattedTime, logType, message);
        }
    }

    private static final List<Log> logger = new ArrayList<>();

    private static final List<Log> loggerForSave = new ArrayList<>();

    private Logger() {
    }

    private static void insertLog(LogType logType, String message) {
        long timestamp = System.currentTimeMillis();
        logger.add(new Log(timestamp, logType, message));
        switch (logType) {
            case START, DEBUG, ERROR, EXIT -> loggerForSave.add(new Log(timestamp, logType, message));
        }
        if (!disabled) {
            save();
        }
    }

    private static void consoleLog(Log log) {
        switch (log.logType) {
            case INFO -> Console.print(log.toString());
            case INPUT -> Console.msg(log.toString());
            case ERROR, WARNING -> Console.error(log.toString());
            case DEBUG -> Console.warning(log.toString());
            case EXIT -> Console.msgSec(log.toString());
        }
    }

    public static void displayLog() {
        logger.forEach(Logger::consoleLog);
    }

    private static void saveLog(Log log) {
        try {
            String msg = log.logType == LogType.START ? String.format("\n%s", log.toString()) : log.toString();
            FileWorker.update(FileWorker.Path.LOG, msg);
        } catch (FileNotFoundException | RuntimeException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static void save() {
        for (int i = 0; i < loggerForSave.size(); i++) {
            Log log = loggerForSave.get(i);
            try {
                saveLog(log);
                loggerForSave.remove(i);
            } catch (RuntimeException ex) {
                save();
            }
        }
    }

    public static void exit(int code) {
        insertLog(LogType.EXIT, String.format("System exit code %d.", code));
    }

    public static void exit() {
        exit(0);
    }


    public static void input(String msg) {
        insertLog(LogType.INPUT, msg);
    }

    public static void info(String msg) {
        insertLog(LogType.INFO, msg);
    }

    public static void warning(String msg) {
        insertLog(LogType.WARNING, msg);
    }

    public static void error(String msg) {
        insertLog(LogType.ERROR, msg);
    }

    public static void debug(String msg) {
        insertLog(LogType.DEBUG, msg);
    }

    public static void start() {
        String sep = "=".repeat(13);
        String msg = String.format("%s Application started %s", sep, sep);
        insertLog(LogType.START, msg);
    }
}
