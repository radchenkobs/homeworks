import HappyFamily.enums.Messages;
import HappyFamily.enums.Species;
import HappyFamily.pet.Cat;
import HappyFamily.pet.Dog;
import org.junit.Test;

import static org.junit.Assert.*;

public class PetTest {
    @Test
    public void toStringTest() {
        Dog dog = new Dog("DogName");
        String msg = dog.toString();
        String expected = "Dog{nickname='DogName', age=0, trickLevel=40, not habits\t can`t fly, 4 legs, has fur}";
        assertEquals("toString empty object state", expected, msg);
    }

    @Test
    public void equalsTest() {
        Dog dog = new Dog("DogName");
        Dog dog1 = dog;
        boolean eqOne = dog.equals(dog1);
        assertTrue("Pet equals test one", eqOne);

        dog1 = null;
        boolean eqTwo = dog.equals(dog1);
        assertFalse("Pet equals test one", eqTwo);
    }

    @Test
    public void nickNameTest() {
        String expectedOne = "Dog";
        Dog dog = new Dog(expectedOne);
        String resOne = dog.getNickname();
        assertEquals("Pet nickName test one", expectedOne, resOne);

        String expectedTwo = "Dogggggggg";
        dog.setNickname(expectedTwo);
        String resTwo = dog.getNickname();
        assertEquals("Pet species test one", expectedTwo, resTwo);

        try {
            dog.setNickname("");
        } catch (RuntimeException ex) {
            String res = ex.getMessage();
            String expMessage = Messages.EMPTY_STRING.message;
            assertEquals("Pet name test exception", expMessage, res);
        }
    }

    @Test
    public void speciesTest() {
        Dog dog = new Dog("DogName");
        Species expectedOne = Species.DOG;
        Species resOne = dog.getSpecies();
        assertEquals("Pet species test one", expectedOne, resOne);
    }

    @Test
    public void ageTest() {
        Dog dog = new Dog("DogName");
        int resOne = dog.getAge();
        int expectedOne = 0;
        assertEquals("Pet age test one", expectedOne, resOne);

        dog.setAge(1);
        int resTwo = dog.getAge();
        int expectedTwo = 1;
        assertEquals("Pet age test two", expectedTwo, resTwo);

        String expMessage = Messages.INVALID_PET_AGE.message;
        try {
            dog.setAge(0);
        } catch (RuntimeException ex) {
            String res = ex.getMessage();
            assertEquals("Pet age test exception min value", expMessage, res);
        }

        try {
            dog.setAge(101);
        } catch (RuntimeException ex) {
            String res = ex.getMessage();
            assertEquals("Pet age test exception max value", expMessage, res);
        }
    }


    @Test
    public void eatTest() {
        Dog pet = new Dog("DogName");
        boolean res = pet.eat();
        assertTrue("Pet eat test", res);
    }

    @Test
    public void respondTest() {
        Dog dog = new Dog("dog");
        String res = dog.respond();
        String expected = "Hi, I am Dog. I missed you!\n";
        assertEquals("Pet respond", expected, res);

        Cat cat = new Cat("cat");
        String res1 = cat.respond();
        String expected1 = "Hi, I am Cat!\n";
        assertEquals("Pet respond", expected1, res1);
    }

    @Test
    public void foulTest() {
        Cat cat = new Cat("Cat");
        boolean res = cat.foul();
        assertTrue("Pet foul test", res);
    }
}
