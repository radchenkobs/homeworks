package libs;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DateUtil {
    private final long timestamp;
    private final LocalDateTime localDateTime;
    private final Date date;

    private DateUtil(long timestamp) {
        this.timestamp = timestamp;
        this.date = new Date(timestamp);
        localDateTime = Instant.ofEpochMilli(this.date.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }


    public static DateUtil of() {
        return new DateUtil(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
    }

    public static DateUtil of(String string) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        long ts = LocalDate.parse(string, formatter)
                .atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
        return new DateUtil(ts);
    }

    public static DateUtil of(long timestamp) {
        return new DateUtil(timestamp);
    }

    public static DateUtil of(Integer year, Integer month, Integer day) {
        if (year == null || month == null || day == null) {
            throw new NullPointerException("Date constructor parameters is null!");
        }
        long timeStamp = LocalDateTime.of(year, month, day, 0, 0).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        return new DateUtil(timeStamp);
    }

    public int hashCode() {
        return new Date(this.timestamp).hashCode();
    }

    public int getDifferentInYear() {
        long between = ChronoUnit.YEARS.between(LocalDateTime.now(), localDateTime);
        return (int) Math.abs(between);
    }

    public int getDifferentInMonth() {
        long between = ChronoUnit.MONTHS.between(LocalDateTime.now(), localDateTime);
        return (int) Math.abs(between);
    }

    public int getDifferentInDay() {
        long between = ChronoUnit.DAYS.between(LocalDateTime.now(), localDateTime);
        return (int) Math.abs(between);
    }

    public Period getDiffPeriod() {
        return Period.between(LocalDate.now(), localDateTime.toLocalDate());
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public int getYear() {
        return localDateTime.getYear();
    }


    public int getMonth() {
        return localDateTime.getMonth().getValue();
    }

    public int getDayOfMonth() {
        return localDateTime.getDayOfMonth();
    }

    public String formatter(String pattern) {
//        yyyy-MM-dd HH:mm:ss
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localDateTime.format(formatter);
    }

    public String formatter(FormatStyle formatStyle) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(formatStyle);
        return localDateTime.format(formatter);
    }

    public static int lengthOfMonth(int year, int month) {
        return YearMonth.of(year, month).lengthOfMonth();
    }
}
