package HappyFamily.app.menu;

import HappyFamily.app.AppController;

import java.util.Stack;

public class MenuStack {
    private static final Stack<Menu> stack = new Stack<Menu>();

    private MenuStack() {
    }

    public static void add(Menu menu) {
        stack.push(menu);
    }

    public static void back() {
        stack.pop();
        if (!isEmpty()) {
            stack.peek().run();
        } else {
            AppController.exit();
        }
    }

    public static int size() {
        return stack.size();
    }

    public static boolean isEmpty() {
        return stack.size() == 0;
    }
}
