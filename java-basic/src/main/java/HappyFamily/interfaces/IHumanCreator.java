package HappyFamily.interfaces;

import HappyFamily.humans.Human;

public interface IHumanCreator {
    Human bornChild();
}
