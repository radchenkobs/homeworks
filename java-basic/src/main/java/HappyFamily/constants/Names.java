package HappyFamily.constants;


import libs.RandomNum;

public class Names {
    private static final String[] male = {"Oliver", "Jack", "Harry", "Jacob", "Charley", "Thomas", "George", "Oscar", "James", "William"};
    private static final String[] female = {"Melanie", "Florence", "Agatha", "Zoe", "Rebecca", "Ruth", "Barbara", "Amanda", "Victoria", "Irene", "Sophia", "Vivian", "Emma"};
    private static final String[] surname = {"Karleone", "Rossi", "Russo", "Romano", "Marino", "Bruno", "Conti", "De Luca", "Mancini", "Costa", "Giordano", "Moretti"};


    public static String getRandomMaleName() {
        int idx = RandomNum.random(0, male.length - 1);
        return male[idx];
    }

    public static String getRandomFemaleName() {
        int idx = RandomNum.random(0, female.length - 1);
        return female[idx];
    }
    
    public static String getRandomSurname() {
        int idx = RandomNum.random(0, female.length - 1);
        return surname[idx];
    }
}
