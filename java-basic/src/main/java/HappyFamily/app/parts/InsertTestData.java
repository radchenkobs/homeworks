package HappyFamily.app.parts;

import HappyFamily.Schedule;
import HappyFamily.constants.Names;
import HappyFamily.enums.DayOfWeek;
import HappyFamily.family.Family;
import HappyFamily.family.FamilyController;
import HappyFamily.humans.Man;
import HappyFamily.humans.Woman;
import HappyFamily.pet.Cat;
import HappyFamily.pet.Dog;
import libs.DateUtil;
import libs.RandomNum;

import java.util.HashSet;
import java.util.Set;

public class InsertTestData extends Part {
    private int testDataCount = 12;

    private InsertTestData(FamilyController controller) {
        super("Insert test data", controller);
    }

    private Schedule createTestSchedule() {
        String[] tasks = new String[]{"fitness", "library", "music", "homework", "busy"};
        Schedule schedule = new Schedule();
        int daysLength = DayOfWeek.values().length;
        int minRandomLength = RandomNum.random(0, 3);
        for (int i = 0; i < RandomNum.random(minRandomLength, daysLength); i++) {
            int dayIdx = minRandomLength <= 1 ? RandomNum.random(0, daysLength) : i;
            DayOfWeek day = DayOfWeek.values()[dayIdx];
            for (int j = 0; j < RandomNum.random(1, tasks.length); j++) {
                String task = tasks[RandomNum.random(0, tasks.length)];
                schedule.setTask(day, task);
            }
        }

        return schedule;
    }

    private Man createTestMan() {
        int year = RandomNum.random(1970, 1980);
        int month = RandomNum.random(1, 12);
        int day = RandomNum.random(1, 28);
        long birthday = DateUtil.of(year, month, day).getTimestamp();
        Schedule schedule = RandomNum.randomSwitch() ? null : createTestSchedule();
        return new Man(
                Names.getRandomMaleName(),
                Names.getRandomSurname(),
                birthday,
                null,
                RandomNum.random(80, 180),
                schedule
        );
    }

    private Woman createTestWoman() {
        int year = RandomNum.random(1970, 1980);
        int month = RandomNum.random(1, 12);
        int day = RandomNum.random(1, 28);
        long birthday = DateUtil.of(year, month, day).getTimestamp();
        Schedule schedule = RandomNum.randomSwitch() ? null : createTestSchedule();
        return new Woman(
                Names.getRandomMaleName(),
                Names.getRandomSurname(),
                birthday,
                null,
                RandomNum.random(80, 180),
                schedule
        );
    }

    private Cat createTestCat() {
        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        return new Cat(
                Names.getRandomMaleName(),
                RandomNum.random(1, 10),
                RandomNum.random(1, 100),
                habits
        );
    }

    private Dog createTestDog() {
        Set<String> habits = new HashSet<>();
        habits.add("play");
        return new Dog(
                Names.getRandomMaleName(),
                RandomNum.random(1, 10),
                RandomNum.random(1, 100),
                habits
        );
    }

    private Family createTestFamily() {
        Man m = createTestMan();
        Woman w = createTestWoman();
        w.setSurname(m.getSurname());
        return new Family(w, m);
    }

    private Family createTestFamilyFull() {
        Man m = createTestMan();
        Woman w = createTestWoman();
        w.setSurname(m.getSurname());
        Family family = new Family(w, m);
        w.bornChild();
        family.addChild(createTestMan());
        family.addPet(createTestCat());
        family.addPet(createTestDog());
        return family;
    }

    public static IPart of(FamilyController controller) {
        return new InsertTestData(controller).instance();
    }

    @Override
    public void run() {
        testDataCount = inputNumber("Enter test family count");
        if (testDataCount == 1) {
            controller.createNewFamily(createTestFamilyFull());
            displayTotal();
            return;
        }
        for (int i = 0; i < testDataCount; i++) {
            Family family = controller.createNewFamily(createTestFamily());
            int id = family.id;
            if (i == 0) continue;
            if (i <= testDataCount / 3) {
                if (RandomNum.randomSwitch()) {
                    controller.addPet(id, createTestCat());
                    controller.bornChildById(id, Names.getRandomMaleName(), Names.getRandomFemaleName());
                } else {
                    controller.addPet(id, createTestDog());
                    controller.adoptChild(family, createTestMan());
                }
                continue;
            }
            if (i <= testDataCount / 2) {
                if (RandomNum.randomSwitch()) {
                    controller.addPet(id, createTestCat());
                } else {
                    controller.addPet(id, createTestDog());
                }
                controller.bornChildById(id, Names.getRandomMaleName(), Names.getRandomFemaleName());
                controller.adoptChild(family, createTestMan());
                continue;
            }

            if (i <= (testDataCount * 0.75)) {
                if (RandomNum.randomSwitch()) {
                    controller.bornChildById(id, Names.getRandomMaleName(), Names.getRandomFemaleName());
                } else {
                    controller.addPet(id, createTestCat());
                }
                continue;
            }

            for (int j = 0; j < (i % 2) + 1; j++) {
                if (RandomNum.randomSwitch()) {
                    controller.adoptChild(family, createTestMan());
                } else {
                    controller.bornChildById(family.id, Names.getRandomMaleName(), Names.getRandomFemaleName());
                }
            }
            if (((i % 2) == 0)) {
                if (RandomNum.randomSwitch()) {
                    controller.addPet(family.id, createTestCat());
                } else {
                    controller.addPet(family.id, createTestDog());
                }
            }
        }
        displayTotal();
    }
}
