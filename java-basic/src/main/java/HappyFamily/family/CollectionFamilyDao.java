package HappyFamily.family;

import HappyFamily.enums.Messages;
import HappyFamily.helpers.Errors;
import HappyFamily.interfaces.IFamilyDao;
import libs.Logger;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements IFamilyDao {
    private final List<Family> data = new ArrayList<>();

    @Override
    public void replaceData(List<Family> familyList) {
        if (data.isEmpty()) {
            loadData(familyList);
            return;
        }
        Logger.debug(String.format("db: Replaced data from arr. Size:%d.", familyList.size()));
        data.clear();
        familyList.forEach(this::saveFamily);
    }

    @Override
    public void loadData(List<Family> familyList) {
        Logger.debug(String.format("db: Load data from arr. Size:%d.", familyList.size()));
        familyList.forEach(this::saveFamily);
    }

    @Override
    public List<Family> getAllFamilies() {
        Logger.debug("db: Get all families");
        return data;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index < 0 || index > data.size()) {
            Errors.runtimeException(Messages.OUT_OF_BOUNDS);
        }
        Family family = data.get(index);
        Logger.debug(String.format("db: Get family by index:%d, id:%d", index, family.id));
        return family;
    }

    @Override
    public Family getFamilyById(int id) {
        if (id < 0) {
            Errors.runtimeException(Messages.OUT_OF_BOUNDS);
        }

        Family family = data.stream().filter(f -> f.id == id).toList().get(0);

        if (family == null) {
            Logger.debug(String.format("db: Get family by id:%d nothing to found", id));
            return null;
        }
        Logger.debug(String.format("db: Get family by id:%d", id));
        return family;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index < 0 || index > data.size()) {
            Errors.runtimeException(Messages.OUT_OF_BOUNDS);
        }
        Family f = data.get(index);
        Family deletedFamily = data.remove(index);
        Logger.debug(String.format("db: Delete family by index:%d, id:%d", index, deletedFamily.id));
        return f.equals(deletedFamily);
    }

    @Override
    public boolean deleteFamily(Family family) {
        Logger.debug(String.format("db: Delete family by instance, id:%d", family.id));
        return data.remove(family);
    }

    @Override
    public boolean deleteFamilyById(int id) {
        Family familyToRemove = data.stream().filter(f -> f.id == id).toList().get(0);
        if (familyToRemove == null) {
            Logger.debug("db: Delete family by id:%d nothing to found");
            return false;
        }
        Logger.debug(String.format("db: Delete family by id:%-5d [_id:%-5d uniqueId:%s]", familyToRemove.id, familyToRemove.id, familyToRemove.uniqueId));
        return data.remove(familyToRemove);
    }

    @Override
    public boolean saveFamily(Family family) {
        if (!data.contains(family)) {
            Logger.debug(String.format("db: Add new family index:%d, ID:%d.", data.size(), family.id));
            return data.add(family);
        }
        int idx = data.indexOf(family);
        data.set(idx, family);
        Logger.debug(String.format("db: Save family index:%d, ID:%d.", idx, family.id));
        return true;
    }

    @Override
    public int count() {
        int c = data.size();
        Logger.debug(String.format("db: Get table size: %d", c));
        return c;
    }
}
