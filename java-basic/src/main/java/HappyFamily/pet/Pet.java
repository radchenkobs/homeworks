package HappyFamily.pet;

import HappyFamily.enums.HumanSex;
import HappyFamily.enums.Species;
import HappyFamily.helpers.CheckValues;
import HappyFamily.helpers.ClassInitializer;
import libs.Console;
import libs.WorkerString;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class Pet extends ClassInitializer {
    private String nickname;
    private final Species species;
    private int age;
    private int trickLevel;
    private Set<String> habits = new HashSet<String>();

    public Pet(String nickname, Species species, int age, int trickLevel, Set<String> habits) {
        this.nickname = CheckValues.emptyString(nickname);
        this.species = species;
        this.age = CheckValues.petAge(age);
        this.trickLevel = CheckValues.petTrickLevel(trickLevel);
        this.habits = habits;
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this(nickname, Species.UNKNOWN, age, trickLevel, habits);
    }

    public Pet(String nickname, int age, int trickLevel) {
        this(nickname, Species.UNKNOWN, age, trickLevel, null);
    }

    public void msg() {
        Console.msgBold(String.format("%s: ", getNickname()));
    }

    @Override
    public String toString() {
        String hab = (habits == null) ? "not habits" : String.format("habits=%s", habits);
        String fly = species.canFly ? "can fly" : "can`t fly";
        String ful = species.hasFur ? "has fur" : "has no fur";
        String legs = species.numberOfLegs == 0 ? "no legs" : String.format("%d legs", species.numberOfLegs);
        String desc = String.format("%s, %s, %s", fly, legs, ful);
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, %s\t %s}", species.value, nickname, age, trickLevel, hab, desc);
    }

    public String prettyFormat() {
        String hab = (habits == null) ? "" : String.format(", habits=%s", habits);
        return String.format("{species=%s, nickname='%s', age=%d, trickLevel=%d%s}", species.name, nickname, age, trickLevel, hab);
    }

    @Override
    public int hashCode() {
        int code = nickname.hashCode() + species.hashCode() + (age * 31) + (trickLevel * 31) + species.hashCode();
        if (habits == null) return code;
        for (String habit : habits) {
            if (habit == null) continue;
            code += habit.hashCode();
        }
        return code;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Pet that = (Pet) obj;
        return that.age == this.age && that.trickLevel == this.trickLevel && that.species.equals(this.species) && that.nickname.equals(this.nickname) && this.habits.equals(that.habits);
    }

    public void setNickname(String nickname) {
        this.nickname = CheckValues.emptyString(nickname);
    }

    public String getNickname() {
        return nickname == null ? "without nickname" : WorkerString.capitalize(nickname);
    }

    public Species getSpecies() {
        return species;
    }

    public void setAge(int age) {
        this.age = CheckValues.petAge(age);
    }

    public int getAge() {
        return age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = CheckValues.petTrickLevel(trickLevel);
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public final boolean eat() {
        msg();
        Console.print("I am eating!\n");
        return true;
    }

    public abstract String respond();
}
