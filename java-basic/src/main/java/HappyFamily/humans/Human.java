package HappyFamily.humans;

import HappyFamily.family.Family;
import HappyFamily.Schedule;
import HappyFamily.enums.HumanSex;
import HappyFamily.helpers.CheckValues;
import HappyFamily.helpers.ClassInitializer;
import HappyFamily.pet.Pet;
import libs.Console;
import libs.DateUtil;
import libs.RandomNum;
import libs.WorkerString;

import java.time.Period;
import java.util.Set;

public abstract class Human extends ClassInitializer {
    private String name;
    private String surname;
    private final long birthDate;
    private int iq;
    private Schedule schedule;
    private Family family;
    private final HumanSex sex;

    public Human(String name, String surname, HumanSex sex, long birthDate, Family family, int iq, Schedule schedule) {
        this.name = name.trim();
        this.surname = surname.trim();
        this.sex = sex;
        this.birthDate = birthDate;
        this.iq = CheckValues.humanIQ(iq);
        this.family = family;
        this.schedule = schedule;
    }

    public Human(String name, String surname, HumanSex sex, String birthDate, Family family, int iq, Schedule schedule) {
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.birthDate = DateUtil.of(birthDate).getTimestamp();
        this.iq = CheckValues.humanIQ(iq);
        this.family = family;
        this.schedule = schedule;
    }

    public Human(String name, String surname, HumanSex sex, Family family, int iq) {
        this(name, surname, sex, System.currentTimeMillis(), family, iq, null);
    }

    public Human(String name, String surname, HumanSex sex, Family family) {
        this(name, surname, sex, family, 100);
    }

    public Human(String name, String surname, HumanSex sex) {
        this(name, surname, sex, null);
    }

    public Human(String name, String surname, HumanSex sex, String birthDate, int iq) {
        this(name, surname, sex, DateUtil.of(birthDate).getTimestamp(), null, iq, null);
    }

    public void msg() {
        Console.msgBold(String.format("%s: ", getFullName()));
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        Console.warning(String.format("Instance %s \"%s\" hashCode: %d REMOVED\n", name, this.getClass().getSimpleName(), hashCode()));
    }

    @Override
    public String toString() {
        String scheduleString = schedule == null ? "none" : String.format("%s", schedule);
        return String.format("%-6s {name='%s', surname='%s', birthday=%s, iq=%d}, schedule=%s}", sex.capitalize(), name, surname, getBirthDate(), iq, scheduleString);
    }

    public String prettyFormat() {
        String scheduleString = schedule == null ? "none" : String.format("%s", schedule);
        return String.format("{name='%s', surname='%s', birthday='%s', iq=%d, schedule=%s}", name, surname, getBirthDate(), iq, scheduleString);
    }

    @Override
    public int hashCode() {
        int scheduleCode = schedule == null ? "schedule".hashCode() : schedule.hashCode();
        int code = DateUtil.of(birthDate).hashCode() + (41 * iq) + scheduleCode + family.hashCode();
        if (name != null) code += name.hashCode();
        if (surname != null) code += surname.hashCode();
        return code;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Human that = (Human) obj;
        return that.birthDate == this.birthDate
                && that.iq == this.iq
                && that.name.equals(this.name)
                && that.sex.equals(this.sex)
                && that.surname.equals(this.surname)
                && schedule != null && that.schedule.equals(this.schedule)
                && that.family.equals(this.family);
    }

    public void setName(String name) {
        this.name = CheckValues.emptyString(name);
    }

    public String getName() {
        return WorkerString.capitalize(name);
    }

    public void setSurname(String surname) {
        this.surname = WorkerString.capitalize(surname);
    }

    public String getSurname() {
        return WorkerString.capitalize(surname);
    }

    public String getFullName() {
        return String.format("%s %s", getName(), getSurname());
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return this.family;
    }

    public void greetPet() {
        msg();
        Family family = getFamily();
        if (family == null || family.getPets() == null || family.getPets().size() == 0) {
            Console.print("I don't have pets!\n");
            return;
        }
        for (Pet pet : family.getPets()) {
            if (pet == null) continue;
            Console.print(String.format("Hi, %s!\n", pet.getNickname()));
        }
    }

    public void describePet() {
        if (family == null || family.getPets() == null && family.getPets().size() == 0) {
            msg();
            Console.print("I don't have pets!\n");
            return;
        }
        for (Pet pet : family.getPets()) {
            if (pet == null) continue;
            msg();
            String trickLevel = pet.getTrickLevel() <= 50 ? "not tricky" : "very clever";
            String msg = String.format("I have a %s, he is a %d year old, %s.\n", pet.getSpecies(), pet.getAge(), trickLevel);
            Console.print(msg);
        }
    }

    public boolean feedPet(Pet pet) {
        boolean status = false;
        if (family != null && family.getPets() != null && family.getPets().size() != 0) {
            Set<Pet> pets = family.getPets();
            for (Pet p : pets) {
                if (!p.equals(pet)) continue;
                p.eat();
                status = true;
            }
        }
        return status;
    }

    public boolean feedPet(boolean isTime) {
        boolean status = false;
        if (family == null || family.getPets() == null || family.getPets().size() == 0) return status;
        if (isTime) {
            for (Pet pet : family.getPets()) {
                if (feedPet(pet)) status = true;
            }
        } else {
            int rNum = RandomNum.random(0, 100);
            for (Pet pet : family.getPets()) {
                msg();
                if (pet.getTrickLevel() < rNum) {
                    Console.print(String.format("Hmm... I'll feed you %s\n", pet.getNickname()));
                    if (feedPet(pet)) status = true;
                } else {
                    Console.print(String.format("I don't think %s is hungry.\n", pet.getNickname()));
                }
            }
        }
        return status;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Schedule getSchedule() {
        return this.schedule;
    }

    public int getIq() {
        return this.iq;
    }

    public int getAge() {
        return DateUtil.of(birthDate).getDifferentInYear();
    }

    public String getBirthDate() {
        return DateUtil.of(birthDate).formatter("dd/MM/yyyy");
    }

    public String describeAge() {
        Period diff = DateUtil.of(birthDate).getDiffPeriod();
        int diffYear = Math.abs(diff.getYears());
        int diffMonth = Math.abs(diff.getMonths());
        int diffDay = Math.abs(diff.getDays());
        return String.format("DIFF: %d-years; %d-months; %d-days;", diffYear, diffMonth, diffDay);
    }

    public HumanSex getSex() {
        return sex;
    }
}
