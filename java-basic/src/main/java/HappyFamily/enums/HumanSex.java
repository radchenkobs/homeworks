package HappyFamily.enums;

import libs.WorkerString;

public enum HumanSex {
    MALE,
    FEMALE;

    public final String value;

    HumanSex() {
        this.value = this.name();
    }

    public String capitalize() {
        return WorkerString.capitalize(this.name());
    }

}
