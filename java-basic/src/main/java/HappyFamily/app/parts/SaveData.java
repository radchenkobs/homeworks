package HappyFamily.app.parts;

import HappyFamily.app.AppController;
import HappyFamily.family.Family;
import HappyFamily.family.FamilyController;
import libs.Console;
import libs.FamiliesFiles;
import libs.Logger;

import java.util.List;

public class SaveData extends Part {
    public SaveData(FamilyController controller) {
        super("Save data", controller, o -> {
            boolean v1 = !o.controller.isEmpty() && !o.controller.getAllFamilies().equals(AppController.getLastLoadingData());
            return v1 && !AppController.equalsLoadAndSaveData();
        });
    }

    public static IPart of(FamilyController controller) {
        return new SaveData(controller).instance();
    }

    @Override
    public void run() {
        try {
            List<Family> dataToSave = controller.getAllFamilies();
            FamiliesFiles.save(dataToSave);
            AppController.setLastSavingData(dataToSave);
        } catch (RuntimeException ex) {
            String msg = String.format("Save data error: %s", ex.getMessage());
            Logger.error(msg);
            Console.error(msg + "\n");
        }
    }
}
