package HappyFamily.pet;

import HappyFamily.enums.Species;
import libs.Console;

import java.util.Set;

public class RoboCat extends Pet {

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, Species.CAT, age, trickLevel, habits);
    }

    public RoboCat(String nickname) {
        this(nickname, 0, 1, null);
    }

    @Override
    public String respond() {
        msg();
        String msg = String.format("Hi, I am %s!\n", getNickname());
        Console.print(msg);
        return msg;
    }
}
