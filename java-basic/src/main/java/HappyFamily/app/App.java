package HappyFamily.app;

import HappyFamily.app.menu.AppMenu;
import HappyFamily.app.menu.Menu;
import HappyFamily.family.FamilyController;
import libs.Logger;

public class App {
    public final FamilyController controller = new FamilyController();
    public Menu menu = new AppMenu(controller);

    public void run() {
        Logger.start();
        menu.run();
    }
}
