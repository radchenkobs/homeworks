package HappyFamily.app.menu;

import HappyFamily.app.parts.*;
import HappyFamily.app.parts.createNewFamily.CreateNewFamily;
import HappyFamily.app.parts.deleteFamily.DeleteFamily;
import HappyFamily.app.parts.editFamily.EditFamily;
import HappyFamily.app.parts.loadData.LoadData;
import HappyFamily.family.FamilyController;

import java.util.HashMap;
import java.util.Map;

public class AppMenu extends Menu {
    public AppMenu(FamilyController controller) {
        super("App Menu");
        Map<Integer, IPart> menu = new HashMap<>();

        menu.put(1, InsertTestData.of(controller));
        menu.put(2, AllFamilies.of(controller));
        menu.put(3, AllFamiliesBiggerThan.of(controller));
        menu.put(4, AllFamiliesLessThan.of(controller));
        menu.put(5, CountFamiliesEqualsMemberNumber.of(controller));
        menu.put(6, CreateNewFamily.of(controller));
        menu.put(7, DeleteFamily.of(controller));
        menu.put(8, EditFamily.of(controller));
        menu.put(9, DeleteChildrenFromAge.of(controller));
        menu.put(10, ClearData.of(controller));
        menu.put(11, LoadData.of(controller));
        menu.put(12, SaveData.of(controller));

        setMenu(menu);
    }
}
