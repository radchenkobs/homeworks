package HappyFamily.interfaces;

import HappyFamily.family.Family;

import java.util.List;

public interface IFamilyDao {
    void replaceData(List<Family> familyList);

    void loadData(List<Family> familyList);

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    Family getFamilyById(int Id);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    boolean deleteFamilyById(int id);

    boolean saveFamily(Family family);

    int count();
}
