package HappyFamily.helpers;

import libs.Console;
import libs.Logger;

public class UserInMessage {

    public static void enterNumberMessage(String message) {
        Console.input(message + "\n");
        Logger.info(message);
    }

    public static void enterMenuNumber() {
        String msg = "Enter menu number";
        Console.input(msg + "\n");
        Logger.info(msg);
    }

    public static void enterFamilyCountNumber() {
        String msg = "Enter family member count number";
        Console.input(msg + "\n");
        Logger.info(msg);
    }


    public static void confirmExit() {
        Console.input("Are you sure you want to exit? (Yes/No)\n");
    }
}
