package HappyFamily.constants;

import libs.DateUtil;

public class Constants {
    public static final int CURRENT_YEAR = DateUtil.of().getYear();
}
