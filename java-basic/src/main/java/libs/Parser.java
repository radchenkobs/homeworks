package libs;

import java.util.Arrays;
import java.util.regex.*;

public class Parser {
    private static boolean hasString(String inputString, String[] checkedStrings) {
        String str = inputString.trim().toLowerCase();
        return Arrays.stream(checkedStrings).anyMatch(ex -> Arrays.asList(str.split(" ")).contains(ex.toLowerCase()));
    }

    public static String parseRegexFormat(String input, String regex) throws RuntimeException {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        boolean status = Pattern.matches(regex, input);
        if (!status) {
            throw new RuntimeException("Error parse format");
        }
        return input;
    }

    public static int parseIntFromString(String a) throws NumberFormatException {
        try {
            return Integer.parseInt(a);
        } catch (NumberFormatException ex) {
            throw new NumberFormatException(ex.getMessage());
        }
    }

    public static int parseIntFromString(String a, int min) throws NumberFormatException {
        try {
            int num = Integer.parseInt(a);
            if (num < min) {
                throw new NumberFormatException(String.format("Min value %d. Input value %d", min, num));
            }
            return num;
        } catch (NumberFormatException ex) {
            throw new NumberFormatException(ex.getMessage());
        }
    }

    public static int parseIntFromString(String a, int min, int max) throws NumberFormatException {
        try {
            int num = Integer.parseInt(a);
            if (num < min) {
                throw new NumberFormatException(String.format("Min value %d. Input value %d", min, num));
            }
            if (num > max) {
                throw new NumberFormatException(String.format("Max value %d. Input value %d", max, num));
            }
            return num;
        } catch (NumberFormatException ex) {
            throw new NumberFormatException(ex.getMessage());
        }
    }

    // TODO: create func for check all number from string
    public static int parseIntFromStringArray(String string) throws NumberFormatException {
        String[] arr = string.split(" ");
        try {
            Integer num = Arrays.stream(arr).map(Integer::parseInt).toList().get(0);
            if (num == null) {
                throw new NumberFormatException();
            }
            return num;
        } catch (NumberFormatException ex) {
            throw new NumberFormatException(ex.getMessage());
        }
    }

    public static boolean parseStringIsExit(String inputString) {
        String[] exitLabels = new String[]{"exit", "ex", "q", "qq", "quit"};
        return hasString(inputString, exitLabels);
    }

    public static boolean parseStringIsBack(String inputString) {
        String[] exitLabels = new String[]{"back", "b",};
        return hasString(inputString, exitLabels);
    }

    public static boolean parseStringIsYes(String inputString) {
        String[] yesLabels = new String[]{"yes", "y", "yep"};
        return hasString(inputString, yesLabels);
    }

    public static boolean parseStringIsNo(String inputString) {
        String[] yesLabels = new String[]{"no", "n", "nope"};
        return hasString(inputString, yesLabels);
    }

    public static boolean parseBooleanFromString(String input) {
        boolean isYes = parseStringIsYes(input);
        boolean isNo = parseStringIsNo(input);
        if (!isYes && !isNo) {
            throw new RuntimeException(String.format("Parse error boolean from string: %s", input));
        }
        return isYes;
    }
}
