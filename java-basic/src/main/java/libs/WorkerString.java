package libs;

public class WorkerString {
    public static String truncate(String text, int length) {
        if (text.length() == 0 || text.length() <= length) return text;
        return text.substring(0, length - 3).concat("...");
    }

    public static String capitalize(String text) {
        char[] chars = text.toCharArray();
        chars[0] = Character.toUpperCase(chars[0]);
        for (int i = 1; i < chars.length; i++) {
            chars[i] = Character.toLowerCase(chars[i]);
        }
        return new String(chars);
    }

    public static String upper(String text) {
        char[] chars = text.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            chars[i] = Character.toUpperCase(chars[i]);
        }
        return new String(chars);
    }
}
