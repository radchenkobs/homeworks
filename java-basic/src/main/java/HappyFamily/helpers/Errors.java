package HappyFamily.helpers;

import HappyFamily.enums.Messages;
import libs.Console;
import libs.Logger;

public class Errors {
    public static void runtimeException(Messages msg) {
        throw new RuntimeException(msg.message);
    }

    public static void runtimeException() {
        throw new RuntimeException(Messages.OOPS.message);
    }

    public static void runtimeException(String msg) {
        throw new RuntimeException(msg);
    }

    public static void inputParseIntException(String str) {
        String msg = String.format("Number format exception. Input: %s", str);
        Console.error(msg + "\n");
        Logger.warning(msg);
    }

    public static void inputOutOfBounds(int num) {
        String msg = String.format("Out of bounds. Input: %s", num);
        Console.error(msg + "\n");
        Logger.warning(msg);
    }

    public static void inputZeroNumber() {
        String msg = "Number must be bigger zero";
        Console.error(msg + "\n");
        Logger.warning(msg);
    }

    public static void inputLessZeroNumber() {
        String msg = "Number must be positive";
        Console.error(msg + "\n");
        Logger.warning(msg);
    }
}
