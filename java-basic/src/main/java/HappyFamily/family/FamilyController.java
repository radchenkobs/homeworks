package HappyFamily.family;

import HappyFamily.humans.Human;
import HappyFamily.pet.Pet;
import libs.Console;
import libs.Logger;

import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService service = new FamilyService();

    public void replaceData(List<Family> familyList) {
        service.replaceData(familyList);
    }

    public void loadData(List<Family> familyList) {
        service.loadData(familyList);
    }

    public List<Family> getAllFamilies() {
        return service.getAllFamilies();
    }

    public void displayAllFamilies() {
        service.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        try {
            return service.getFamiliesBiggerThan(count);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return null;
    }

    public List<Family> getFamiliesLessThan(int count) {
        try {
            return service.getFamiliesLessThan(count);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return null;
    }

    public int countFamiliesWithMemberNumber(int count) {
        try {
            return service.countFamiliesWithMemberNumber(count);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return 0;
    }

    public Family createNewFamily(Human mother, Human father) {
        return service.createNewFamily(new Family(mother, father));
    }

    public Family createNewFamily(Family family) {
        return service.createNewFamily(family);
    }

    public boolean deleteFamilyByIndex(int index) {
        try {
            return service.deleteFamilyByIndex(index);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return false;
    }

    public boolean deleteFamilyById(int id) {
        try {
            return service.deleteFamilyById(id);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return false;
    }

    public Human bornChild(Family family, String mName, String fName) {
        try {
            return service.bornChild(family, mName, fName);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return null;
    }

    public Human bornChild(int index, String mName, String fName) throws FamilyOverflowException {
        try {
            return service.bornChild(index - 1, mName, fName);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return null;
    }

    public Human bornChildById(int id, String mName, String fName) throws FamilyOverflowException {
        try {
            return service.bornChildById(id, mName, fName);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return null;
    }

    public void adoptChild(Family family, Human child) throws FamilyOverflowException {
        try {
            service.adoptChild(family, child);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
    }

    public void adoptChild(int index, Human child) throws FamilyOverflowException {
        try {
            service.adoptChild(index - 1, child);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
    }

    public int deleteAllChildrenOlderThen(int age) {
        try {
            return service.deleteAllChildrenOlderThen(age).size();
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return 0;
    }

    public int size() {
        return service.count();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public Family getFamilyByIndex(int index) {
        try {
            return service.getFamilyByIndex(index);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return null;
    }

    public Family getFamilyById(int id) {
        try {
            return service.getFamilyById(id);
        } catch (FamilyOverflowException ex) {
            Console.error(String.format("%s\n", ex.getMessage()));
            Logger.error(ex.getMessage());
        }
        return null;
    }

    public Set<Pet> getPets(int id) {
        return service.getPets(id);
    }

    public boolean addPet(int id, Pet pet) {
        return service.addPet(id, pet);
    }

}
