package HappyFamily.humans;

import HappyFamily.family.Family;
import HappyFamily.Schedule;
import HappyFamily.enums.HumanSex;
import HappyFamily.pet.Pet;
import libs.Console;

public final class Man extends Human {

    public Man(String name, String surname, long birthDate, Family family, int iq, Schedule schedule) {
        super(name, surname, HumanSex.MALE, birthDate, family, iq, schedule);
    }

    public Man(String name, String surname, Family family, int iq) {
        super(name, surname, HumanSex.MALE, family, iq);
    }

    public Man(String name, String surname) {
        super(name, surname, HumanSex.MALE);
    }

    public Man(String name, String surname, String birthDate, int iq, Schedule schedule) {
        super(name, surname, HumanSex.MALE, birthDate, null, iq, schedule);
    }

    public Man(String name, String surname, String birthDate, int iq) {
        this(name, surname, birthDate, iq, null);
    }

    @Override
    public void greetPet() {
        msg();
        Family family = getFamily();
        if (family == null || family.getPets() == null || family.getPets().size() == 0) {
            Console.print("I don't have pets!\n");
            return;
        }
        for (Pet pet : family.getPets()) {
            if (pet == null) continue;
            Console.print(String.format("Hi, %s!\n", pet.getNickname()));
        }
    }

    public void repairCar() {
        msg();
        System.out.println("Repair car...");
    }
}
