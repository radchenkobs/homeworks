package HappyFamily.app.parts;

import HappyFamily.family.Family;
import HappyFamily.family.FamilyController;
import HappyFamily.helpers.Errors;
import HappyFamily.helpers.UserInMessage;
import libs.Console;
import libs.Input;
import libs.Logger;
import libs.Parser;

import java.util.ArrayList;
import java.util.List;

public class AllFamiliesBiggerThan extends Part {

    public AllFamiliesBiggerThan(FamilyController controller) {
        super("Show list of families bigger than number", controller, o -> !o.controller.isEmpty());
    }

    public static IPart of(FamilyController controller) {
        return new AllFamiliesBiggerThan(controller).instance();
    }

    @Override
    public void run() {
        int num = inputFamilyCountNumber();
        Logger.input(String.format("%d", num));
        List<Family> familiesBiggerThan = controller.getFamiliesBiggerThan(num);
        displayTotal(familiesBiggerThan.size());
        displayFamilyList(familiesBiggerThan);
        if (familiesBiggerThan.size() > 2) {
            displayTotal(familiesBiggerThan.size());
        }
    }
}
