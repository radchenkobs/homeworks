package HappyFamily.app.parts;

import HappyFamily.family.Family;
import HappyFamily.family.FamilyController;
import libs.Console;
import libs.Logger;

import java.util.List;

public interface IPart {

    String title = null;
    FamilyController controller = null;
    boolean disabled = false;

    String getTitle();

    default void displayTitle() {
        Logger.info(String.format("Open %s", getTitle()));
        Console.msgSec(String.format("%s:\n", getTitle()));
    }

    boolean isDisabled();

    void displayTotal();

    void displayFamilyList(List<Family> familyList);

    int inputFamilyCountNumber();

    int inputNumber(String msg);

    void run();

    IPart instance();
}
