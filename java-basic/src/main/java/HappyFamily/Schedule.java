package HappyFamily;

import HappyFamily.enums.DayOfWeek;
import libs.Console;

import java.util.*;

public class Schedule {
    public final class Tasks {
        private List<String> tasks = new ArrayList<String>();

        public Tasks(String task) {
            this.tasks.add(task);
        }

        public Tasks(String[] tasks) {
            Collections.addAll(this.tasks, tasks);
        }

        public Tasks(List<String> tasks) {
            this.tasks = tasks;
        }

        public Tasks add(String task) {
            tasks.add(task);
            return this;
        }

        public Tasks add(String[] tasks) {
            this.tasks.addAll(Arrays.asList(tasks));
            return this;
        }

        public Tasks add(List<String> tasks) {
            this.tasks.addAll(tasks);
            return this;
        }

        public List<String> getTasks() {
            return tasks;
        }

        @Override
        public String toString() {
            if (tasks == null) return "none";
            StringBuilder msg = new StringBuilder();
            msg.append('[');
            for (int i = 0; i < tasks.size(); i++) {
                msg.append(String.format("%s", tasks.get(i)));
                if (i < tasks.size() - 1) {
                    msg.append(", ");
                }
            }
            msg.append(']');
            return new String(msg);
        }

        @Override
        public int hashCode() {
            int code = "tasks".hashCode();
            if (tasks == null) return code;
            for (String task : tasks) {
                code += task.hashCode();
            }
            return code;
        }

        public int count() {
            return tasks.size();
        }
    }

    private final Map<DayOfWeek, Tasks> schedule;

    public Schedule() {
        this.schedule = new HashMap<DayOfWeek, Tasks>();
    }

    public Schedule(Map<DayOfWeek, Tasks> schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        String msg = "none";
        if (schedule == null || schedule.size() == 0) return msg;
        StringBuilder str = new StringBuilder();
        str.append("{");
        DayOfWeek[] days = DayOfWeek.values();
        int count = 0;
        for (int i = 0; i < days.length; i++) {
            DayOfWeek day = days[i];
            if (schedule.get(day) == null) continue;
            if (count != 0) {
                str.append("; ");
            }
            Tasks tasks = schedule.get(day);
            String s = String.format("%s (%d): %s", day.format.capitalize, tasks.count(), tasks.toString());
            str.append(s);
            count++;
        }
        str.append("}");
        return new String(str);
    }

    @Override
    public int hashCode() {
        int code = "schedule".hashCode();
        if (schedule != null) {
            for (Map.Entry<DayOfWeek, Tasks> entry : schedule.entrySet()) {
                if (entry == null) continue;
                code += entry.getKey().hashCode() + entry.getValue().hashCode();
            }
        }
        return code;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Schedule that = (Schedule) obj;
        return this.schedule.equals(that.schedule);
    }

    public boolean setTask(DayOfWeek day, String task) {
        if (task.length() == 0) return false;
        Tasks tasks = schedule.get(day);
        if (tasks == null) {
            schedule.put(day, new Tasks(task));
            return true;
        }
        if (tasks.getTasks().contains(task)) return false;
        schedule.put(day, tasks.add(task));
        return true;
    }

    public boolean setTask(DayOfWeek day, String[] tasks) {
        if (tasks.length == 0) return false;
        Tasks currentTask = schedule.get(day);
        if (currentTask == null) {
            schedule.put(day, new Tasks(tasks));
            return true;
        }
        schedule.put(day, currentTask.add(tasks));
        return true;
    }

    public void setTask(DayOfWeek day, List<String> tasks) {
        Tasks t = schedule.get(day);
        if (t == null) {
            schedule.put(day, new Tasks(tasks));
        } else {
            schedule.put(day, t.add(tasks));
        }
    }

    public String[] getTasks(DayOfWeek day) {
        Tasks tasks = schedule.get(day);
        if (tasks == null) return new String[0];
        String[] result = new String[tasks.getTasks().size()];
        for (int i = 0; i < tasks.getTasks().size(); i++) {
            result[i] = tasks.getTasks().get(i);
        }
        return result;
    }

    public Map<DayOfWeek, Tasks> getAll() {
        return this.schedule;
    }

}
