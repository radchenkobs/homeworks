# homeworks

- ### Basic
    - #### [homework1](https://gitlab.com/radchenkobs/homeworks/-/tree/hw1/java-basic/src/main/java/homework1)
    - #### [homework2](https://gitlab.com/radchenkobs/homeworks/-/tree/hw2/java-basic/src/main/java/homework2)
    - #### [homework3](https://gitlab.com/radchenkobs/homeworks/-/tree/hw3/java-basic/src/main/java/homework3)
    - #### [homework4](https://gitlab.com/radchenkobs/homeworks/-/tree/hw4/java-basic/src/main/java/homework4)
    - #### [homework5](https://gitlab.com/radchenkobs/homeworks/-/tree/hw5/java-basic/src/main/java/homework5)
    - #### [homework6](https://gitlab.com/radchenkobs/homeworks/-/tree/hw6/java-basic/src/main/java/homework6)
    - #### [homework7](https://gitlab.com/radchenkobs/homeworks/-/tree/hw7/java-basic/src/main/java/homework7)
    - #### [homework8](https://gitlab.com/radchenkobs/homeworks/-/tree/hw8/java-basic/src/main/java/HappyFamily)
    - #### [homework9](https://gitlab.com/radchenkobs/homeworks/-/tree/hw9/java-basic/src/main/java/HappyFamily)
    - #### [homework10](https://gitlab.com/radchenkobs/homeworks/-/tree/hw10/java-basic/src/main/java/HappyFamily)
    - #### [homework11](https://gitlab.com/radchenkobs/homeworks/-/tree/hw11/java-basic/src/main/java/HappyFamily)
    - #### [homework12](https://gitlab.com/radchenkobs/homeworks/-/tree/hw12/java-basic/src/main/java/HappyFamily)
- ### Web
- ### Algorithm
- ### Frameworks