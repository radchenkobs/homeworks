package HappyFamily.interfaces;

public interface IPetCanFoul {
    boolean foul();
}
