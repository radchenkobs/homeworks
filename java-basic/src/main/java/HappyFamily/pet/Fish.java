package HappyFamily.pet;

import HappyFamily.enums.Species;
import libs.Console;

import java.util.Set;

public class Fish extends Pet {
    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, Species.FISH, age, trickLevel, habits);
    }

    public Fish(String nickname) {
        this(nickname, 0, 10, null);
    }

    @Override
    public String respond() {
        msg();
        String msg = "\n";
        Console.print(msg);
        return msg;
    }
}
