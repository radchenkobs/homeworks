package HappyFamily.app.stepper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Stepper {
    private final List<Step<?>> steps = new ArrayList<>();

    public void add(Step step) {
        steps.add(step);
    }

    public Step<?> get(int stepIndex) {
        return steps.get(stepIndex);
    }

    public void run() {
        steps.forEach(Step::in);
    }

    public void run(int step) {
        steps.get(step).in();
    }

    public void runFor(int stepCount) {
        for (int i = 0; i < steps.size(); i++) {
            if (i >= stepCount) continue;
            steps.get(i).in();
        }
    }

    public void runFrom(int stepCount) {
        for (int i = 0; i < steps.size(); i++) {
            if (i <= stepCount) continue;
            steps.get(i).in();
        }
    }
}
