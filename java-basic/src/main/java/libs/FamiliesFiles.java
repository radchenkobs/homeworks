package libs;

import HappyFamily.Schedule;
import HappyFamily.enums.DayOfWeek;
import HappyFamily.enums.HumanSex;
import HappyFamily.family.Family;
import HappyFamily.humans.Human;
import HappyFamily.humans.Man;
import HappyFamily.humans.Woman;
import HappyFamily.pet.Pet;
import org.json.JSONArray;
import org.json.JSONObject;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FamiliesFiles {

    enum Keys {
        MOTHER("mother"),
        FATHER("father"),
        CHILDREN("children"),
        PETS("pets"),
        UId("uniqueId"),
        NAME("name"),
        SURNAME("surname"),
        BIRTHDAY("birthday"),
        IQ("iq"),
        SEX("s"),
        SCHEDULE("schedule");

        final String key;

        Keys(String key) {
            this.key = key;
        }
    }

    record HumanData(String name, String surname, String birthday, int iq, Schedule schedule, String uniqueId,
                     String sex) {
    }

    private static JSONObject createPetObject(Pet pet) {
        JSONArray habits = new JSONArray();
        pet.getHabits().forEach(habits::put);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("uniqueId", pet.uniqueId);
        jsonObject.put("nickname", pet.getNickname());
        jsonObject.put("species", pet.getSpecies().name);
        jsonObject.put("age", pet.getAge());
        jsonObject.put("trickLevel", pet.getTrickLevel());
        jsonObject.put("habits", habits);
        return jsonObject;
    }

    private static JSONObject createHumanObject(Human human) {
        JSONObject schedule = new JSONObject();
        if (human.getSchedule() != null) {
            human.getSchedule().getAll().forEach((d, t) -> {
                JSONArray tasksForDay = new JSONArray();
                t.getTasks().forEach(tasksForDay::put);
                schedule.put(d.name, tasksForDay);
            });
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Keys.UId.key, human.uniqueId);
        jsonObject.put(Keys.NAME.key, human.getName());
        jsonObject.put(Keys.SURNAME.key, human.getSurname());
        jsonObject.put(Keys.BIRTHDAY.key, human.getBirthDate());
        jsonObject.put(Keys.IQ.key, human.getIq());
        jsonObject.put(Keys.SCHEDULE.key, schedule);
        jsonObject.put(Keys.SEX.key, human.getSex());
        return jsonObject;
    }

    private static JSONObject createFamilyObject(Family family) {
        JSONObject jsonObject = new JSONObject();
        JSONArray children = new JSONArray();
        JSONArray pets = new JSONArray();

        family.getChildren().forEach(child -> children.put(createHumanObject(child)));
        family.getPets().forEach(pet -> pets.put(createPetObject(pet)));

        jsonObject.put(Keys.UId.key, family.uniqueId);
        jsonObject.put(Keys.MOTHER.key, createHumanObject(family.getMother()));
        jsonObject.put(Keys.FATHER.key, createHumanObject(family.getFather()));
        jsonObject.put(Keys.CHILDREN.key, children);
        jsonObject.put(Keys.PETS.key, pets);

        return jsonObject;
    }

    public static void save(List<Family> familyList) throws RuntimeException {
        try {
            JSONArray list = new JSONArray();
            familyList.forEach(family -> list.put(createFamilyObject(family)));

            FileWorker.write(FileWorker.Path.FAMILIES, list.toString());
        } catch (RuntimeException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    private static Schedule createHumanScheduleDataFromJson(String string) {
        JSONObject scheduleJson = new JSONObject(string);
        Schedule schedule = new Schedule();
        Arrays.stream(DayOfWeek.values()).forEach(d -> {
            try {
                JSONArray arr = (JSONArray) scheduleJson.get(d.name);
                List<String> list = IntStream.range(0, arr.length())
                        .mapToObj(arr::get)
                        .map(Object::toString)
                        .collect(Collectors.toList());
                schedule.setTask(d, list);
            } catch (RuntimeException ignored) {
            }
        });

        return schedule;
    }

    private static HumanData createHumanDataFromJson(String string) {
        JSONObject humanJsonObject = new JSONObject(string);

        String uId = humanJsonObject.getString(Keys.UId.key);

        String name = humanJsonObject.getString(Keys.NAME.key);
        String surname = humanJsonObject.getString(Keys.SURNAME.key);
        String birthday = humanJsonObject.getString(Keys.BIRTHDAY.key);
        int iq = humanJsonObject.getInt(Keys.IQ.key);
        String sex = humanJsonObject.getString(Keys.SEX.key);
        Schedule schedule = createHumanScheduleDataFromJson(humanJsonObject.getJSONObject(Keys.SCHEDULE.key).toString());

        return new HumanData(name, surname, birthday, iq, schedule, uId, sex);
    }

    private static Family createFamilyFromJson(String string) {
        JSONObject familyJsonObject = new JSONObject(string);

        HumanData motherData = createHumanDataFromJson(familyJsonObject.getJSONObject(Keys.MOTHER.key).toString());
        Woman mother = new Woman(
                motherData.name,
                motherData.surname,
                motherData.birthday,
                motherData.iq,
                motherData.schedule
        );
        mother.setUId(motherData.uniqueId);

        HumanData fatherData = createHumanDataFromJson(familyJsonObject.getJSONObject(Keys.FATHER.key).toString());
        Man father = new Man(
                fatherData.name,
                fatherData.surname,
                fatherData.birthday,
                fatherData.iq,
                fatherData.schedule
        );
        father.setUId(fatherData.uniqueId);

        Family family = new Family(mother, father);
        family.setUId(familyJsonObject.getString(Keys.UId.key));

        JSONArray children = familyJsonObject.getJSONArray(Keys.CHILDREN.key);
        children.forEach(c -> {
            HumanData childData = createHumanDataFromJson(c.toString());
            Human child;
            if (HumanSex.MALE.value.equals(childData.sex)) {
                child = new Man(
                        childData.name,
                        childData.surname,
                        childData.birthday,
                        childData.iq,
                        childData.schedule
                );
            } else {
                child = new Woman(
                        childData.name,
                        childData.surname,
                        childData.birthday,
                        childData.iq,
                        childData.schedule
                );
            }
            family.addChild(child);
        });

        return family;
    }

    public static List<Family> read(String path) throws RuntimeException {
        try {
            List<Family> familyList = new ArrayList<>();
            String content;

            try {
                content = FileWorker.read(path);
                if (content.isBlank()) {
                    throw new RuntimeException("Nothing to load");
                }
            } catch (FileNotFoundException ex) {
                throw new RuntimeException(ex.getMessage());
            }

            JSONArray list = new JSONArray(content);

            list.forEach(o -> familyList.add(createFamilyFromJson(o.toString())));

            return familyList;

        } catch (RuntimeException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<Family> read() throws RuntimeException {
        return read(FileWorker.Path.FAMILIES.name);
    }

    private static void listFilesForFolder(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                System.out.println(fileEntry.getName());
            }
        }
    }

    public static void listFiles() throws IOException {
//        final File folder = new File("assets");
//        listFilesForFolder(folder);
        
        try (Stream<Path> paths = Files.walk(Paths.get("assets"))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(System.out::println);
        }
    }


}
