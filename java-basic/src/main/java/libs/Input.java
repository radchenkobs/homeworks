package libs;

public class Input {
    public static String string() {
        var scanner = new ScannerConstrained();
        return scanner.nextLine().trim();
    }

    public static int integer() throws NumberFormatException {
        var scanner = new ScannerConstrained();
        String line = scanner.nextLine();
        try {
            return Integer.parseInt(line);
        } catch (NumberFormatException ex) {
            throw new NumberFormatException(ex.getMessage());
        }
    }
}
