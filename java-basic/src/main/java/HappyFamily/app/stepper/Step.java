package HappyFamily.app.stepper;

import libs.Console;
import libs.Input;
import libs.Logger;

import java.util.function.Function;

public class Step<R> {
    private String value = null;
    private final String title;
    private boolean isDone = false;
    private Function<String, R> validator = null;
    private R validateValue = null;

    public Step(String title) {
        this.title = title;
    }

    public Step(String title, Function<String, R> validator) {
        this.title = title;
        this.validator = validator;
    }

    public void setValidator(Function<String, R> validator) {
        this.validator = validator;
    }

    public String getValue() {
        return this.value;
    }

    public String getTitle() {
        return this.title;
    }

    public R getValidateValue() {
        return validator.apply(this.value);
    }

    private void input() {
        value = Input.string().trim();
        if (validator == null) return;
        try {
            validateValue = validator.apply(value);
        } catch (RuntimeException ex) {
            String msg = String.format("Step input string validate; Error: %s", ex.getMessage());
            Console.error(msg + "\n");
            Logger.warning(msg);
            input();
        }
    }

    public void in() {
        Console.input(title + "\n");
        input();
    }
}

