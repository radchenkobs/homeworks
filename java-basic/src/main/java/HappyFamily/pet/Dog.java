package HappyFamily.pet;

import HappyFamily.enums.Species;
import HappyFamily.interfaces.IPetCanFoul;
import libs.Console;

import java.util.Set;

public class Dog extends Pet implements IPetCanFoul {

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, Species.DOG, age, trickLevel, habits);
    }

    public Dog(String nickname) {
        this(nickname, 0, 40, null);
    }

    @Override
    public String respond() {
        msg();
        String msg = String.format("Hi, I am %s. I missed you!\n", getNickname());
        Console.print(msg);
        return msg;
    }

    @Override
    public boolean foul() {
        msg();
        Console.print("I need to cover up my tracks...\n");
        return true;
    }
}
