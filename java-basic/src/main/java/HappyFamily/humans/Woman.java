package HappyFamily.humans;

import HappyFamily.family.Family;
import HappyFamily.Schedule;
import HappyFamily.constants.Names;
import HappyFamily.helpers.Errors;
import HappyFamily.enums.HumanSex;
import HappyFamily.interfaces.IHumanCreator;
import HappyFamily.pet.Pet;
import libs.Console;
import libs.RandomNum;

public final class Woman extends Human implements IHumanCreator {

    public Woman(String name, String surname, long birthDate, Family family, int iq, Schedule schedule) {
        super(name, surname, HumanSex.FEMALE, birthDate, family, iq, schedule);
    }

    public Woman(String name, String surname, Family family, int iq) {
        super(name, surname, HumanSex.FEMALE, family, iq);
    }

    public Woman(String name, String surname, String birthDate, int iq, Schedule schedule) {
        super(name, surname, HumanSex.FEMALE, birthDate, null, iq, schedule);
    }

    public Woman(String name, String surname, String birthDate, int iq) {
        this(name, surname, birthDate, iq, null);
    }

    public Woman(String name, String surname) {
        super(name, surname, HumanSex.FEMALE);
    }

    @Override
    public void greetPet() {
        msg();
        Family family = getFamily();
        if (family == null || family.getPets() == null || family.getPets().size() == 0) {
            Console.print("I don't have pets!\n");
            return;
        }
        for (Pet pet : family.getPets()) {
            if (pet == null) continue;
            Console.print(String.format("Hi, %s!\n", pet.getNickname()));
        }
    }

    @Override
    public Human bornChild() {
        Family family = getFamily();
        if (family == null) {
            Errors.runtimeException();
        }
        Woman mother = family.getMother();
        Man father = family.getFather();
        int iq = (father.getIq() + mother.getIq()) / 2;
        String surname = father.getSurname();
        Human child;
        if (RandomNum.randomSwitch()) {
            child = new Man(Names.getRandomMaleName(), surname, family, iq);
        } else {
            child = new Woman(Names.getRandomFemaleName(), surname, family, iq);
        }
        family.addChild(child);
        return child;
    }

    public void makeup() {
        msg();
        System.out.println("Makeup ...");
    }
}
