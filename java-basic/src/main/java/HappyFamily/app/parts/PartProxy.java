package HappyFamily.app.parts;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class PartProxy implements InvocationHandler {
    private Part obj;

    PartProxy(Part obj) {
        this.obj = obj;
    }


    public static IPart newInstance(Part obj) {
        return (IPart) Proxy.newProxyInstance(
                Part.class.getClassLoader(),
                Part.class.getInterfaces(),
                new PartProxy(obj));
    }


    public void runValidator() {
        if (obj.validator != null) {
            Boolean isValid = obj.validator.apply(obj);
            obj.setIsDisabled(!isValid);
        }
    }

    @Override
    public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {
        Object result;
        try {
            runValidator();
            boolean isRun = m.getName().startsWith("run");
            if (isRun) {
                obj.displayTitle();
            }

            result = m.invoke(obj, args);

            runValidator();
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        } catch (Exception e) {
            throw new RuntimeException("unexpected invocation exception: " + e.getMessage());
        }
        return result;
    }
}
