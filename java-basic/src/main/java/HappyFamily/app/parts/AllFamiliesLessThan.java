package HappyFamily.app.parts;

import HappyFamily.family.Family;
import HappyFamily.family.FamilyController;
import HappyFamily.helpers.Errors;
import HappyFamily.helpers.UserInMessage;
import libs.Console;
import libs.Input;
import libs.Logger;
import libs.Parser;

import java.util.List;

public class AllFamiliesLessThan extends Part {

    public AllFamiliesLessThan(FamilyController controller) {
        super("Show list of families less than number", controller, o -> !o.controller.isEmpty());
    }

    public static IPart of(FamilyController controller) {
        return new AllFamiliesLessThan(controller).instance();
    }

    @Override
    public void run() {
        int num = inputFamilyCountNumber();
        Logger.input(String.format("%d", num));
        List<Family> familiesLessThan = controller.getFamiliesLessThan(num);
        displayTotal(familiesLessThan.size());
        displayFamilyList(familiesLessThan);
        if (familiesLessThan.size() > 2) {
            displayTotal(familiesLessThan.size());
        }
    }
}
