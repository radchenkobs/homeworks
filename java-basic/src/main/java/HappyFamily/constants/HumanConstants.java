package HappyFamily.constants;

public class HumanConstants {
    public static final int HUMAN_MIN_YEAR = 1900;
    public static final int HUMAN_MAX_YEAR = Constants.CURRENT_YEAR;
    public static final int HUMAN_MIN_IQ = 0;
    public static final int HUMAN_MAX_IQ = 200;
}
