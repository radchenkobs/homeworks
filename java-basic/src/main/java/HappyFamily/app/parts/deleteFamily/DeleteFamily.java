package HappyFamily.app.parts.deleteFamily;

import HappyFamily.app.menu.DeleteFamilyMenu;
import HappyFamily.app.menu.Menu;
import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.InsertTestData;
import HappyFamily.app.parts.Part;
import HappyFamily.constants.Names;
import HappyFamily.family.Family;
import HappyFamily.family.FamilyController;
import HappyFamily.humans.Man;
import HappyFamily.humans.Woman;
import libs.DateUtil;
import libs.RandomNum;

public class DeleteFamily extends Part {

    private final Menu menu = new DeleteFamilyMenu(controller);

    public DeleteFamily(FamilyController controller) {
        super("Delete family", controller, o -> !o.controller.isEmpty());
    }

    public static IPart of(FamilyController controller) {
        return new DeleteFamily(controller).instance();
    }

    @Override
    public void run() {
        menu.run();
    }
}
