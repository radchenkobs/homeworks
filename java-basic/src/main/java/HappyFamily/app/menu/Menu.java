package HappyFamily.app.menu;

import HappyFamily.app.AppController;
import HappyFamily.app.parts.IPart;
import HappyFamily.app.parts.Part;
import HappyFamily.helpers.Errors;
import HappyFamily.helpers.UserInMessage;
import libs.Console;
import libs.Input;
import libs.Logger;
import libs.Parser;

import java.util.Map;

public abstract class Menu {
    private Map<Integer, IPart> parts;
    public final String title;
    private boolean repeat = true;

    public Menu(String title, boolean repeat) {
        this.title = title;
        this.repeat = repeat;
    }

    public Menu(String title) {
        this(title, true);
    }

    public void setMenu(Map<Integer, IPart> menu) {
        this.parts = menu;
    }

    public int inputMenuNumber() {
        UserInMessage.enterMenuNumber();
        String str = Input.string().trim();
        if (Parser.parseStringIsExit(str) && !AppController.exit()) {
            return inputMenuNumber();
        }
        if (Parser.parseStringIsBack(str)) {
            return -1;
        }
        try {
            int num = Parser.parseIntFromString(str);
            if (num <= 0 || num > parts.size()) {
                Errors.inputOutOfBounds(num);
                return inputMenuNumber();
            }
            return num;
        } catch (NumberFormatException ex) {
            Errors.inputParseIntException(str);
            return inputMenuNumber();
        }
    }

    private void displayMenuItem(int num, IPart part) {
        boolean disabled = part.isDisabled();
        String start = "- ";
        String meg = String.format("%s. ", num);
        String title = String.format("%s", part.getTitle());
        String end = ";\n";

        Console.print(start);
        if (disabled) {
            Console.msgSec(meg);
            Console.msgSec(title);
        } else {
            Console.msg(meg);
            Console.title(title);
        }
        Console.print(end);
    }

    private void show() {
        Console.printLine();

        parts.forEach(this::displayMenuItem);

        if (repeat && MenuStack.size() != 1) {
            Console.print("- or ");
            Console.title("back");
            Console.print(";\n");
        }
        Console.print("- or ");
        Console.title("exit");
        Console.print(";\n\n");

        int num = inputMenuNumber();

        if (num == -1) MenuStack.back();

        if (num != -1) {
            IPart part = parts.get(num);
            Logger.input(String.format("Input menu num: %d. Disabled: %b", num, part.disabled));
            if (!part.isDisabled()) {
                part.run();
            } else {
                String msg = String.format("Menu: %d is disabled.", num);
                Console.error(msg + "\n");
                Logger.warning(msg);
            }
        }

        if (repeat && num != -1) show();
    }

    public void run() {
        MenuStack.add(this);
        show();
    }
}
