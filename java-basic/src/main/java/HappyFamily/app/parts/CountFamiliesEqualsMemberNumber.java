package HappyFamily.app.parts;

import HappyFamily.family.FamilyController;
import libs.Console;
import libs.Logger;

public class CountFamiliesEqualsMemberNumber extends Part {

    public CountFamiliesEqualsMemberNumber(FamilyController controller) {
        super("Show count of families with member number", controller, o -> !o.controller.isEmpty());
    }

    public static IPart of(FamilyController controller) {
        return new CountFamiliesEqualsMemberNumber(controller).instance();
    }

    @Override
    public void run() {
        int num = inputFamilyCountNumber();
        Logger.input(String.format("%d", num));
        int count = controller.countFamiliesWithMemberNumber(num);
        Console.print(String.format("%d families with %d members", count, num));
    }
}
