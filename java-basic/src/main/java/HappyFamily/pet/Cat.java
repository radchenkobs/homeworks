package HappyFamily.pet;

import HappyFamily.enums.Species;
import HappyFamily.interfaces.IPetCanFoul;
import libs.Console;

import java.util.Set;

public class Cat extends Pet implements IPetCanFoul {

    public Cat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, Species.CAT, age, trickLevel, habits);
    }

    public Cat(String nickname) {
        this(nickname, 0, 60, null);
    }

    @Override
    public String respond() {
        msg();
        String msg = String.format("Hi, I am %s!\n", getNickname());
        Console.print(msg);
        return msg;
    }

    @Override
    public boolean foul() {
        msg();
        Console.print("I need to cover up my tracks...\n");
        return true;
    }
}
